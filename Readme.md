# DEPRECATED

### THIS PROJECT IS OUTDATED!
The project was the first one developed on Gothic Together platform. It was mostly using for testing the core of the platform. You can still use it as an example of how to achive something in Gothic game but many aspects like the file structure or core modules were changed. Please check our current up to date GameModule called Survival GameMode or the boilerplate starter project.

### Links
https://gitlab.com/gothictogether/boilerplate
https://gitlab.com/gothictogether/survival-gamemode
import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { ConfigureBrowserIfNotInsideGame } from './utils/electron-debugger'
import { HashRouter as Router, Routes, Route } from 'react-router-dom'
import { WebBrowserWrapper } from './components/WebBrowserWrapper'
import OverlayPage from './pages/Overlay'
import HomePage from './pages/Home'
import EquipmentPage from './pages/Equipment'
import HeroPage from './pages/Hero'
import CreatorPage from './pages/Creator'
import ShopPage from './pages/Shop'
import PreloadPage from './pages/PreloadPage'

const isWebBrowser = ConfigureBrowserIfNotInsideGame()
const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

document.addEventListener('keydown', (e) => {
  if (e.key === 'Tab') {
    e.preventDefault()
  }
})

root.render(
  <WebBrowserWrapper active={isWebBrowser}>
    <Router>
      <Routes>
        <Route path="/overlay" element={<OverlayPage />} />
        <Route path="/" element={<HomePage />} />
        <Route path="/hero" element={<HeroPage />} />
        <Route path="/eq" element={<EquipmentPage />} />
        <Route path="/creator" element={<CreatorPage />} />
        <Route path="/shop" element={<ShopPage />} />
        <Route path="/preload" element={<PreloadPage />} />
      </Routes>
    </Router>
  </WebBrowserWrapper>,
)

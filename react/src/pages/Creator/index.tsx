import React from 'react'
import { TooltipProvider } from '../../components/ui/tooltip'
import { sendGameCommand, useGameState } from '../../utils/electron'
import HeaderImg from './assets/h2.png'
import LeftArrowImg from './assets/arrow-left.png'
import RightArrowImg from './assets/arrow-right.png'
import ButtonImg from './assets/BTN.png'

import {
  ArrowSelector,
  Bar,
  BoxSelector,
  Button,
  Column,
  InterColumn,
  LeftArrow,
  PageWrapper,
  RightArrow,
  SectionContainer,
  SectionTitle,
  SelectorText,
  Title,
  Image,
} from './styled'

export type CreatorState = {
  username: string
  gender: 0 | 1
  currBodyVar: number
  face: number
  faceShape: number
  fatness: number
}

const Creator = () => {
  const playerAttr = useGameState<CreatorState>('CreatorComponent')

  if (!playerAttr) {
    return null
  }

  const username = playerAttr.username || 'N/A'

  return (
    <TooltipProvider delayDuration={100} skipDelayDuration={100}>
      <Title>{username}</Title>
      <PageWrapper>
        <Column>
          <SectionContainer>
            <SectionTitle>GENDER</SectionTitle>
            <Bar src={HeaderImg} alt="" />
            <BoxSelector>
              <Image
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: 0,
                    gender: 1,
                    face: 0,
                    faceShape: 0,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={require('./assets/male' + (playerAttr.gender ? '-chosen' : '') + '.png')}
                alt=""
              />
              <Image
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: 0,
                    gender: 0,
                    face: 0,
                    faceShape: 0,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={require('./assets/female' + (playerAttr.gender ? '' : '-chosen') + '.png')}
                alt=""
              />
            </BoxSelector>
            <SectionTitle>BODY</SectionTitle>
            <Bar src={HeaderImg} alt="" />
            <ArrowSelector>
              <LeftArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness > -3 ? playerAttr.fatness - 1 : 3,
                  })
                }}
                src={LeftArrowImg}
                alt=""
              />
              <SelectorText>FATNESS ({playerAttr.fatness + 3}) </SelectorText>
              <RightArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness < 3 ? playerAttr.fatness + 1 : -3,
                  })
                }}
                src={RightArrowImg}
                alt=""
              />
            </ArrowSelector>
          </SectionContainer>
        </Column>
        <InterColumn>
          <Button
            onClick={() => {
              sendGameCommand('ExitCreator')
            }}
          >
            <div>
              <span>READY</span>
            </div>
            <img src={ButtonImg} alt="" />
          </Button>
        </InterColumn>
        <Column>
          <SectionContainer>
            <SectionTitle>APPEARANCE</SectionTitle>
            <Bar src={HeaderImg} alt="" />
            <ArrowSelector>
              <LeftArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar:
                      playerAttr.currBodyVar > 0
                        ? playerAttr.currBodyVar - 1
                        : 5 + playerAttr.gender,
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={LeftArrowImg}
                alt=""
              />
              <SelectorText>
                TEXTURE ({playerAttr.currBodyVar + 1} / {playerAttr.gender ? 7 : 6})
              </SelectorText>
              <RightArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: (playerAttr.currBodyVar + 1) % (6 + playerAttr.gender),
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={RightArrowImg}
                alt=""
              />
            </ArrowSelector>
            <Bar src={HeaderImg} alt="" />
            <ArrowSelector>
              <LeftArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: playerAttr.face > 0 ? playerAttr.face - 1 : playerAttr.gender ? 12 : 20,
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={LeftArrowImg}
                alt=""
              />
              <SelectorText>
                FACE ({playerAttr.face + 1} / {playerAttr.gender ? 13 : 21})
              </SelectorText>
              <RightArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: (playerAttr.face + 1) % (playerAttr.gender ? 13 : 21),
                    faceShape: playerAttr.faceShape,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={RightArrowImg}
                alt=""
              />
            </ArrowSelector>
            <Bar src={HeaderImg} alt="" />
            <ArrowSelector>
              <LeftArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape:
                      playerAttr.faceShape > 0
                        ? playerAttr.faceShape - 1
                        : playerAttr.gender
                          ? 4
                          : 9,
                    fatness: playerAttr.fatness,
                  })
                }}
                src={LeftArrowImg}
                alt=""
              />
              <SelectorText>
                FACE SHAPE ({playerAttr.faceShape + 1} / {playerAttr.gender ? 5 : 10})
              </SelectorText>
              <RightArrow
                onClick={() => {
                  sendGameCommand('CreatorUpdate', {
                    currBodyVar: playerAttr.currBodyVar,
                    gender: playerAttr.gender,
                    face: playerAttr.face,
                    faceShape: (playerAttr.faceShape + 1) % (playerAttr.gender ? 5 : 10),
                    fatness: playerAttr.fatness,
                  })
                }}
                src={RightArrowImg}
                alt=""
              />
            </ArrowSelector>
          </SectionContainer>
        </Column>
      </PageWrapper>
    </TooltipProvider>
  )
}

export default Creator

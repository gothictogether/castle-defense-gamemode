import styled from 'styled-components'

export const PageWrapper = styled.div`
  display: flex;
  height: 100%;
`

export const SectionContainer = styled.div`
  margin: 15px;
  max-width: 500px;
`

export const SelectorText = styled.div`
  font-size: 30px;
  text-align: center;
  width: 90%;
  margin: 15px;
`

export const BoxSelector = styled.div`
  display: flex;
  justify-content: center;
  margin: auto;
  width: 75%;
  margin-bottom: 50px;
`

export const ArrowSelector = styled.div`
  display: flex;
  margin: auto;
  width: 100%;
`

export const InterColumn = styled.div`
  flex: 30%;
  display: flex;
  flex-direction: column;
  justify-content: end;
  align-items: center;
`

export const Column = styled.div`
  flex: 35%;
  justify-content: center;
  display: flex;
`

export const SectionTitle = styled.div`
  font-size: 45px;
  text-align: center;
  width: 100%;
  margin: 10px 0;
  // padding: 5px 0;
  // border-style: solid;
  // border-width: 1px;
`

export const Title = styled.div`
  font-size: 60px;
  text-align: center;
  width: 100%;
`

export const Image = styled.img`
  margin: 0 10px;
`

export const LeftArrow = styled.img`
  float: left;
  margin: 15px;
`

export const RightArrow = styled.img`
  float: right;
  margin: 15px;
`

export const Bar = styled.img`
  margin-left: auto;
  margin-right: auto;
  margin-top: 5px;
  margin-bottom: 5px;
  width: 75%;
`

export const Button = styled.div`
  position: relative;
  width: 287px;
  height: 94px;
  margin-bottom: 150px;

  img {
    filter: brightness(70%);
  }

  div {
    position: absolute;
    top: 0;
    left: 0;
    z-index: 999;
    width: 287px;
    height: 94px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
  }

  span {
    font-size: 40px;
    font-weight: 500;
  }
`

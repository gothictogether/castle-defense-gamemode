import React from 'react'
import { sendGameCommand, useGameState } from '../../utils/electron'

export type AppState = {
  currentWave: number
}

const HomePage = () => {
  const homePageState = useGameState<AppState>('HomePage')
  if(!homePageState) {
    return null
  }

  return (
    <div>
      <section>
        <div className="px-4 mx-auto max-w-screen-xl text-center py-10">
          <h1 className="mb-4 text-4xl font-extrabold tracking-tight leading-none text-white md:text-5xl lg:text-6xl">
            Gothic Together
          </h1>
          <p className="mb-8 text-lg font-normal text-gray-300 lg:text-xl sm:px-16 lg:px-48">
            Misja: Obroń Zamek W Górniczej Dolinie
          </p>
          <div className="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0 sm:space-x-4">
            <button
              onClick={() => {
                sendGameCommand('StartGame')
              }}
              className="inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-white rounded-lg bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900"
            >
              {homePageState.currentWave === 0 ? 'Rozpocznij' : 'Gotowy'}
              <svg
                aria-hidden="true"
                className="ml-2 -mr-1 w-4 h-4"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </button>
            <button
              onClick={() => {
                sendGameCommand('exitgame')
              }}
              className="inline-flex justify-center hover:text-gray-900 items-center py-3 px-5 text-base font-medium text-center text-white rounded-lg border border-white hover:bg-gray-100 focus:ring-4 focus:ring-gray-400"
            >
              Wyjdź z gry
            </button>
          </div>
        </div>
      </section>
    </div>
  )
}

export default HomePage

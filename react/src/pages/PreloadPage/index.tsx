import React, { useEffect } from 'react'
import itemsJson from '../../assets/items.json'

const PreloadPage = () => {
  useEffect(() => {
    const prefetchItems = itemsJson

    const intervalId = setInterval(() => {
      const currerntItem = prefetchItems.shift()
      if (!currerntItem) {
        clearInterval(intervalId)
      } else if (currerntItem.objectName) {
        new Image().src = `/vob-images/${currerntItem.fileName}.png`
      }
    }, 10)

    return () => clearInterval(intervalId)
  }, [])

  return <div></div>
}

export default PreloadPage

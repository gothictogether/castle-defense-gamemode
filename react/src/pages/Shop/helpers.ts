import itemsJson from '../../assets/items.json'
import { sendGameCommand } from '../../utils/electron'
import { getWeaponType, JsonItem } from '../../utils/item-helpers'

export const getItemsByCategoryPrefix = (jsonItems: JsonItem[], category: string) => {
  return jsonItems.filter((item) => item.objectName.startsWith(category))
}

export const getItemsByCategory = (category: string) => {
  if (category === 'ITMI') {
    const filteredAmulets = getItemsByCategoryPrefix(itemsJson, 'ITAM')
    const filteredRings = getItemsByCategoryPrefix(itemsJson, 'ITRI')
    const filteredItems: JsonItem[] = [...filteredAmulets, ...filteredRings]

    return filteredItems
  } else {
    const filteredItems: JsonItem[] = getItemsByCategoryPrefix(itemsJson, category)

    if (category === 'ITMW') {
      const filtered1HWeapons: JsonItem[] = filteredItems.filter(
        (i) => getWeaponType(i.fileName) === 'Broń jednoręczna',
      )
      const filtered2HWeapons: JsonItem[] = filteredItems.filter(
        (i) => getWeaponType(i.fileName) === 'Broń dwuręczna',
      )
      return [...filtered2HWeapons, ...filtered1HWeapons]
    } else if (category === 'ITRW') {
      const filteredBowWeapons: JsonItem[] = filteredItems.filter(
        (i) => getWeaponType(i.fileName) === 'Łuk',
      )
      const filteredCrossbowWeapons: JsonItem[] = filteredItems.filter(
        (i) => getWeaponType(i.fileName) === 'Kusza',
      )
      return [...filteredBowWeapons, ...filteredCrossbowWeapons]
    } else {
      return getItemsByCategoryPrefix(itemsJson, category)
    }
  }
}

export const filterItemsFromAllCategories = () => {
  const filteredAmuletsAndRings = getItemsByCategory('ITMI')
  const filteredWeapons = getItemsByCategory('ITMW')
  const filteredRangedWeapons = getItemsByCategory('ITRW')
  const filteredArmor = getItemsByCategory('ITAR')
  const filteredPotions = getItemsByCategory('ITPO')
  const filteredRunes = getItemsByCategory('ITRU')

  return [
    ...filteredAmuletsAndRings,
    ...filteredArmor,
    ...filteredWeapons,
    ...filteredRangedWeapons,
    ...filteredPotions,
    ...filteredRunes,
  ]
}

export const purchaseItem = (i: JsonItem) => {
  sendGameCommand('PurchaseItem', i.objectName)
}

export const categories = [
  { key: 'ITMW', label: 'Wręcz' },
  { key: 'ITRW', label: 'Dystans' },
  { key: 'ITAR', label: 'Pancerz' },
  { key: 'ITPO', label: 'Mikstury' },
  { key: 'ITRU', label: 'Magia' },
  { key: 'ITMI', label: 'Inne' },
]

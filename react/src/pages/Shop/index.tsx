import React, { useEffect, useState } from 'react'
import { useGameState } from '../../utils/electron'
import { Window } from '../../components/Window'
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '../../components/ui/tooltip'
import { JsonItem, objectNameToJsonItemMap } from '../../utils/item-helpers'
import { ConfirmationBox } from '../../components/ConfirmationBox'
import {
  GoldBar,
  GoldImage,
  ItemCategoryButton,
  ItemCategoryButtonText,
  ItemCategoryContainer,
  ItemImage,
  Items,
  ItemSlot,
  PageWrapper,
  SearchBarContainer,
  SearchInput,
} from './styled'
import { debounce } from '../../utils/debounce'
import {
  categories,
  filterItemsFromAllCategories,
  getItemsByCategory,
  purchaseItem,
} from './helpers'
import GoldImg from '../../assets/gold.png'
import { ItemDescription } from '../../components/ItemDetails'
import { TooltipWindow } from '../../components/TooltipWindow'

export type ShopState = {
  playerGold: number
}

const Shop = () => {
  const [shopItems, setShopItems] = useState<JsonItem[]>([])
  const [selectedCategory, setSelectedCategory] = useState<string>('ITMW')
  const [searchQuery, setSearchQuery] = useState<string>('')
  const [confirmationBox, setConfirmationBox] = useState<boolean>(false)
  const [selectedItem, setSelectedItem] = useState<JsonItem | null>(null)

  const shopData = useGameState<ShopState>('ShopComponent')

  useEffect(() => {
    const filteredItemsOnCategory = getItemsByCategory(selectedCategory)
    setShopItems(filteredItemsOnCategory)
    setConfirmationBox(false)
  }, [selectedCategory])

  const debouncedSearch = debounce((query: string) => {
    const itemsFromAllCategories = filterItemsFromAllCategories()
    const filteredItemsLowerCase = itemsFromAllCategories.filter((item) =>
      item.name.toLowerCase().includes(query.toLowerCase()),
    )
    setShopItems(filteredItemsLowerCase)
    setSelectedCategory('')
  }, 300)

  const inputHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const lowerCase = e.target.value.toLowerCase()
    debouncedSearch(lowerCase)
    setSearchQuery(lowerCase)
  }

  if (!shopData) {
    return null
  }

  return (
    <TooltipProvider delayDuration={100} skipDelayDuration={100}>
      <PageWrapper>
        <Window
          scrollableContent={false}
          title="Sklep"
          width={473}
          leftContent={
            <GoldBar>
              <GoldImage src={GoldImg} alt="" />
              {shopData!.playerGold}
            </GoldBar>
          }
          rightContent={
            <SearchBarContainer>
              <SearchInput
                type="text"
                placeholder="Szukaj..."
                value={searchQuery}
                onChange={inputHandler}
              />
            </SearchBarContainer>
          }
        >
          <ItemCategoryContainer>
            {categories.map((category) => (
              <ItemCategoryButton
                $isselected={selectedCategory === category.key}
                key={category.key}
                onClick={() => setSelectedCategory(category.key)}
              >
                <ItemCategoryButtonText>{category.label}</ItemCategoryButtonText>
              </ItemCategoryButton>
            ))}
          </ItemCategoryContainer>
          <Items>
            {shopItems.map((invItem) => {
              const item = objectNameToJsonItemMap.get(invItem.objectName)

              if (!item || item.objectName === 'ITMI_GOLD') {
                return null
              }

              return (
                <Tooltip key={item.objectName}>
                  <ItemSlot
                    onClick={() => {
                      setSelectedItem(item)
                      if (item.price >= 100) {
                        setConfirmationBox(true)
                      } else {
                        purchaseItem(item)
                      }
                    }}
                  >
                    <TooltipTrigger>
                      <ItemImage src={`/vob-images/${item.fileName}.png`} alt="" />
                    </TooltipTrigger>
                    <TooltipContent onClick={(e) => e.stopPropagation()}>
                      <TooltipWindow
                        item={item}
                        content={<ItemDescription item={item}></ItemDescription>}
                        iconImage={<GoldImage src={GoldImg} alt="" />}
                      ></TooltipWindow>
                    </TooltipContent>
                  </ItemSlot>
                </Tooltip>
              )
            })}
          </Items>
        </Window>
        <>
          {confirmationBox ? (
            <ConfirmationBox
              title={'Sklep'}
              text={`Czy na pewno chcesz kupić ${selectedItem!.name} za ${selectedItem!.price} złota?`}
              onClose={() => setConfirmationBox(false)}
              onYes={() => {
                purchaseItem(selectedItem!)
                setConfirmationBox(false)
              }}
              onNo={() => setConfirmationBox(false)}
            />
          ) : null}
        </>
      </PageWrapper>
    </TooltipProvider>
  )
}

export default Shop

import styled from 'styled-components'
import { COLOR_GOLD, COLOR_LIGHT } from '../../utils/color-constants'
import ItemSlotImg from '../../assets/item-slot.png'

export const PageWrapper = styled.div`
  margin-top: 200px;
  margin-left: 200px;
`

export const ItemCategoryContainer = styled.div`
  display: flex;
  flex-wrap: row;
  align-items: center;
  justify-content: center;
  height: 2.5rem;
`
export const SearchBarContainer = styled.div`
  display: flex;
  justify-content: center;
`
export const SearchInput = styled.input`
  text-align: center;
  padding: 10px;
  margin-top: 1rem;
  border-radius: 4px;
  width: 100%;
  box-sizing: border-box;
  height: 15px;
  background-color: rgba(24, 24, 24, 0.6);
  border: none;
  font-size: 12px;
  color: ${COLOR_LIGHT};
  text-transform: uppercase;
  outline: none;
`

export const ItemCategoryButton = styled.div<{ $isselected: boolean }>`
  height: 5%;
  margin: 10px;
  color: ${({ $isselected }) => ($isselected ? 'rgb(178, 143, 59)' : { COLOR_LIGHT })};
`

export const ItemCategoryButtonText = styled.span`
  display: block;
  width: 100%;
  height: 100%;
  line-height: 4%;
  font-size: 16px;
  text-align: center;
  text-transform: uppercase;
  display: flex;
  justify-content: center;
  &:hover {
    text-shadow: 0 0 10px rgba(255, 255, 255, 0.8);
  }
`
export const Items = styled.div`
  display: flex;
  padding-bottom: 45px;
  flex-wrap: wrap;
  height: 100%;
  align-content: start;
  overflow: scroll;
  &::-webkit-scrollbar {
    display: none;
  }
`

export const ItemSlot = styled.div`
  width: 64px;
  height: 64px;
  margin: 3px;
  padding: 5px;
  background-image: url(${ItemSlotImg});
  &:hover {
    box-shadow: 0 0 10px rgba(255, 255, 255, 0.8);
  }
  background-size: cover;
`

export const ItemImage = styled.img`
  filter: grayscale(0.5);
`

export const GoldBar = styled.div`
  display: flex;
  align-items: center;
  color: ${COLOR_GOLD};
  height: 100%;
  margin-left: 20px;
`

export const GoldImage = styled.img``

import { Item } from '../../utils/item-helpers'

export const itemCategorySortPriority: { [key: string]: number } = {
  ITMW: 1,
  ITRW: 2,
  ITPO: 3,
  ITFO: 4,
  ITAT: 5,
  ITPL: 6,
  ITAR: 7,
  ITAL: 8,
  ITRI: 9,
  ITBE: 10,
  ITRU: 11,
  ITSC: 12,
}

export const sortInventory = (inventory: Item[]) => {
  inventory.sort((a, b) => {
    const categoryA = a.category || 'null'
    const categoryB = b.category || 'null'
    const priorityA = itemCategorySortPriority[categoryA] || 100
    const priorityB = itemCategorySortPriority[categoryB] || 100

    return priorityA - priorityB
  })
}

import React, { useEffect, useRef, useState } from 'react'
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '../../components/ui/tooltip'
import { Window } from '../../components/Window'
import { sendGameCommand, useGameState } from '../../utils/electron'
import { Item, JsonItem, objectNameToJsonItemMap } from '../../utils/item-helpers'
import { ConfirmationBox } from '../../components/ConfirmationBox'
import { useOutsideClick } from '../../utils/use-outside-click'
import {
  ContainerBadge,
  GoldBar,
  GoldImage,
  ItemAmountBadge,
  ItemImage,
  Items,
  ItemSlot,
  PageWrapper,
} from './styled'
import { sortInventory } from './helpers'
import GoldImg from '../../assets/gold.png'
import DropdownMenu from './DropdownMenu'
import { ItemDescription } from '../../components/ItemDetails'
import { TooltipWindow } from '../../components/TooltipWindow'

export type InventoryState = {
  inventory: Item[]
  hotbarSlots: (Item | null)[]
}

const Equipment = () => {
  const inventoryData = useGameState<InventoryState>('InventoryComponent')
  const [sortedInventory, setSortedInventory] = useState<Item[]>([])
  const [confirmationBox, setConfirmationBox] = useState<boolean>(false)
  const [selectedItem, setSelectedItem] = useState<JsonItem | null>(null)
  const [dropDownItem, setDropDownItem] = useState<string>('')
  const dropdownRef = useRef<HTMLDivElement>(null)
  const [selectedHotBarKey, setSelectedHotBarKey] = useState<number | null>(null)
  const [hoveredItem, setHoveredItem] = useState<JsonItem | null>(null)

  useOutsideClick(
    dropdownRef,
    () => {
      setDropDownItem('')
    },
    [],
  )

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key >= '0' && event.key <= '9') {
        setSelectedHotBarKey(parseInt(event.key))
      }
    }
    document.addEventListener('keydown', handleKeyDown)

    return () => {
      document.removeEventListener('keydown', handleKeyDown)
    }
  }, [])

  useEffect(() => {
    if (selectedHotBarKey !== null && selectedHotBarKey > 0 && hoveredItem) {
      sendGameCommand('AddItemToHotBar', {
        objectName: hoveredItem.objectName,
        index: selectedHotBarKey - 1,
      })
      setSelectedHotBarKey(null)
    } else if (selectedHotBarKey !== null && selectedHotBarKey === 0 && hoveredItem) {
      sendGameCommand('RemoveItem', hoveredItem.objectName)
      setSelectedHotBarKey(null)
      setHoveredItem(null)
    }
  }, [selectedHotBarKey, hoveredItem])

  useEffect(() => {
    if (inventoryData) {
      sortInventory(inventoryData.inventory)
      setSortedInventory(inventoryData.inventory)
    }
  }, [inventoryData?.inventory])

  if (!inventoryData) {
    return null
  }

  return (
    <TooltipProvider delayDuration={100} skipDelayDuration={100}>
      <PageWrapper>
        <Window
          scrollableContent={false}
          title="Ekwipunek"
          width={473}
          leftContent={
            <GoldBar>
              <GoldImage src={GoldImg} alt="" />
              {sortedInventory.find((item) => item.objectName === 'ITMI_GOLD')?.amount ??
                0}
            </GoldBar>
          }
        >
          <Items>
            {sortedInventory.map((invItem) => {
              const item = objectNameToJsonItemMap.get(invItem.objectName)
              const amount = invItem.amount || 1

              if (!item || item.objectName === 'ITMI_GOLD') {
                return null
              }

              return (
                <Tooltip key={item.objectName}>
                  <ItemSlot
                    onClick={() => {
                      sendGameCommand('UseItem', item.objectName)
                    }}
                    onMouseOver={() => {
                      setHoveredItem(item)
                    }}
                    onMouseOut={() => {
                      setHoveredItem(null)
                    }}
                    onContextMenu={(e) => {
                      setDropDownItem(item.objectName)
                    }}
                    $isequipped={invItem.isEquipped}
                  >
                    <TooltipTrigger>
                      <ItemImage src={`/vob-images/${item.fileName}.png`} alt="" />
                      <ContainerBadge>
                        {amount > 1 && <ItemAmountBadge>{amount}</ItemAmountBadge>}
                      </ContainerBadge>
                    </TooltipTrigger>
                    <TooltipContent>
                      <TooltipWindow
                        item={item}
                        content={<ItemDescription item={item}></ItemDescription>}
                        amount={amount}
                        iconImage={<GoldImage src={GoldImg} alt="" />}
                      ></TooltipWindow>
                    </TooltipContent>
                  </ItemSlot>
                  {dropDownItem === item.objectName && (
                    <DropdownMenu
                      item={item}
                      isVisible={dropDownItem === item.objectName}
                      setSelectedItem={setSelectedItem}
                      setConfirmationBox={setConfirmationBox}
                    />
                  )}
                </Tooltip>
              )
            })}
          </Items>
        </Window>
        <>
          {confirmationBox ? (
            <ConfirmationBox
              title={'Ekwipunek'}
              text={`Czy na pewno chcesz usunąć ${selectedItem!.name} z ekwipunku?`}
              onClose={() => setConfirmationBox(false)}
              onYes={() => {
                sendGameCommand('DropItem', selectedItem!.objectName)
                setConfirmationBox(false)
              }}
              onNo={() => setConfirmationBox(false)}
            />
          ) : null}
        </>
      </PageWrapper>
    </TooltipProvider>
  )
}

export default Equipment

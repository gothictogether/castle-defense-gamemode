import React from 'react'
import {
  Dropdown,
  DropdownMenuContainer,
  DropDownMenuHeader,
  DropDownMenuBody,
  DropdownMenuButton,
} from './styled'
import { JsonItem } from '../../utils/item-helpers'

type Props = {
  item: JsonItem
  isVisible: boolean
  setSelectedItem: (item: JsonItem | null) => void
  setConfirmationBox: (value: boolean) => void
}

const DropdownMenu = ({
  item,
  isVisible,
  setSelectedItem,
  setConfirmationBox,
}: Props) => (
  <Dropdown>
    <DropdownMenuContainer isVisible={isVisible}>
      <DropDownMenuHeader>{item.name}</DropDownMenuHeader>
      <DropDownMenuBody>
        <DropdownMenuButton
          onClick={(e) => {
            e.preventDefault()
            setSelectedItem(item)
            setConfirmationBox(true)
          }}
        >
          Wyrzuć
        </DropdownMenuButton>
      </DropDownMenuBody>
    </DropdownMenuContainer>
  </Dropdown>
)

export default DropdownMenu

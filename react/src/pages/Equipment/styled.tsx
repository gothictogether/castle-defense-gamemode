import styled from 'styled-components'
import {
  COLOR_DARK_TRANSPARENT,
  COLOR_DARK_YELLOW_TRANSPARENT,
  COLOR_GOLD,
  COLOR_LIGHT,
} from '../../utils/color-constants'
import ItemSlotImg from '../../assets/item-slot.png'

export const PageWrapper = styled.div`
  margin-top: 200px;
  margin-left: 200px;
`

export const Items = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  align-content: start;
  overflow: scroll;
  &::-webkit-scrollbar {
    display: none;
  }
`

export const Dropdown = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-content: center;
  margin-top: 20px;
`

export const DropdownMenuContainer = styled.div<{ isVisible: boolean }>`
  z-index: 4;
  width: 140px;
  top: calc(100% + 0.25rem);
  padding: 0.75rem;
  border-radius: 0.25rem;
  transform: translateX(-30px);

  animation-name: slideDown;
  animation-timing-function: linear;
  animation-duration: 150ms;
  animation-fill-mode: forwards;

  @keyframes slideDown {
    0% {
      opacity: 0;
      transform: translateY(-10px);
    }

    100% {
      opacity: 1;
      transform: translateY(0px);
    }
  }
`

export const DropDownMenuHeader = styled.div`
  background-color: ${COLOR_DARK_YELLOW_TRANSPARENT};
  text-align: center;
  font-size: 0.8rem;
  padding: 2px;
`

export const DropDownMenuBody = styled.div`
  background-color: ${COLOR_DARK_TRANSPARENT};
  display: flex;
  flex-direction: column;
  align-items: start;
  margin-left: 4px;
  padding: 4px;
  gap: 4px;
`
export const DropdownMenuButton = styled.button`
  font-size: 0.8rem;

  &:hover {
    text-shadow: 0 0 10px rgba(255, 255, 255, 0.8);
  }
`

export const ItemSlot = styled.div<{ $isequipped: boolean }>`
  position: relative;
  width: 64px;
  height: 64px;
  margin: 3px;
  padding: 5px;

  &:hover {
    box-shadow: 0 0 10px rgba(255, 255, 255, 0.8);
  }
  ${(props) =>
    props.$isequipped &&
    `
    background: linear-gradient(0deg, rgb(83 3 3 / 30%), rgba(83, 3, 3, 0.3)), url(${ItemSlotImg});
`}
  ${(props) =>
    !props.$isequipped &&
    `
  background-image: url(${ItemSlotImg});
`}
  background-size: cover;
`

export const ItemImage = styled.img`
  filter: grayscale(0.5);
`

export const ContainerBadge = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
  padding: 5px;
  pointer-events: none;
  text-align: right;
`
export const ItemAmountBadge = styled.div`
  color: ${COLOR_LIGHT};
  font-size: 0.8rem;
  height: 15px;
  width: 40px;
`

export const GoldBar = styled.div`
  display: flex;
  align-items: center;
  color: ${COLOR_GOLD};
  height: 100%;
  margin-left: 20px;
`

export const GoldImage = styled.img``

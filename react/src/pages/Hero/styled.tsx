import styled from 'styled-components'

export const PageWrapper = styled.div`
  margin-top: 200px;
  margin-left: 200px;
`

export const AvaliablePoints = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  margin-left: 10px;
`

export const AttributeBox = styled.div`
  width: 100%;
  height: 60px;
  background-color: #0e0e0de8;
  padding: 10px;
  display: flex;
  margin-bottom: 10px;
`

export const AttributeImage = styled.img`
  height: 100%;
  width: auto;
  margin-right: 10px;
`

export const AttributeName = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const Name = styled.div`
  color: #e7ab6e;
  font-size: 16px;
  line-height: 16px;
`

export const Description = styled.div`
  font-size: 13px;
  line-height: 13px;
  color: #c5bcb1;
  margin-right: 10px;
`

export const Points = styled.div`
  width: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: -5px;
  color: #ffa128;
  font-size: 20px;
  margin-left: 8px;
  position: absolute;
  right: -10px;
`

export const Line = styled.div`
  height: 100%;
  border-left: 1px solid #1d1c1b;
  position: absolute;
  right: 43px;
`

export const AddButton = styled.img`
  width: 20px;
  height: 20px;
  cursor: pointer;
  position: absolute;
  right: 55px;
`

export const PointsWrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  width: 100px;
`

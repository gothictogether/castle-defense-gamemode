import React from 'react'
import { Window } from '../../components/Window'
import { sendGameCommand, useGameState } from '../../utils/electron'
import {
  AddButton,
  AttributeBox,
  AttributeImage,
  AttributeName,
  AvaliablePoints,
  Description,
  Line,
  Name,
  PageWrapper,
  Points,
  PointsWrapper,
} from './styled'
import { attributes } from './helpers'
import AddImg from './assets/add.png'

export type HeroPageState = {
  lp: number
  hitpoints: number
  mana: number
  strength: number
  dexterity: number
  '1h': number
  '2h': number
  bow: number
  cbow: number
  mageCircle: number
}

const HeroPage = () => {
  const heroAttributes = useGameState<HeroPageState>('HeroComponent')

  if (!heroAttributes) {
    return null
  }

  return (
    <PageWrapper>
      <Window
        scrollableContent={true}
        title="Postać"
        leftContent={<AvaliablePoints>PN: {heroAttributes.lp}</AvaliablePoints>}
      >
        {attributes.map((a) => (
          <AttributeBox key={a.icon}>
            <AttributeImage src={require(`./assets/icons/${a.icon}.png`)} />
            <AttributeName>
              <Name>{a.name}</Name>
              <Description>{a.description}</Description>
            </AttributeName>
            <PointsWrapper>
              <AddButton
                src={AddImg}
                alt=""
                onClick={() => sendGameCommand('heroUpgrade', { id: a.id })}
              />
              <Line />
              <Points>{heroAttributes[a.id as keyof HeroPageState]}</Points>
            </PointsWrapper>
          </AttributeBox>
        ))}
      </Window>
    </PageWrapper>
  )
}

export default HeroPage

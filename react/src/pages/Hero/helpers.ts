export const attributes = [
  {
    id: 'hitpoints',
    icon: 'Barbarian_5',
    name: 'Punkty Życia',
    description: 'Zdolność postaci do wytrzymywania obrażeń.',
  },
  {
    id: 'mana',
    icon: 'Barbarian_12',
    name: 'Punkty Many',
    description: 'Ilość energii wykorzystywanej do rzucania zaklęć.',
  },
  {
    id: 'strength',
    icon: 'Barbarian_6',
    name: 'Siła',
    description: 'Wpływa na zdolność zadawania większych obrażeń fizycznych.',
  },
  {
    id: 'dexterity',
    icon: 'Barbarian_7',
    name: 'Zręczność',
    description: 'Kluczowa dla celności i skuteczności używania broni dystansowej.',
  },
  {
    id: '1h',
    icon: 'Barbarian_18',
    name: 'Broń Jednoręczna',
    description: 'Umiejętność efektywnego używania lekkiej broni.',
  },
  {
    id: '2h',
    icon: 'Barbarian_22',
    name: 'Broń Dwuręczna',
    description: 'Zdolność do walki cięższymi, dwuręcznymi rodzajami broni.',
  },
  {
    id: 'bow',
    icon: 'Barbarian_30',
    name: 'Łuk',
    description: 'Umiejętność strzelania z łuku, zwiększająca precyzję trafień.',
  },
  {
    id: 'cbow',
    icon: 'Barbarian_37',
    name: 'Kusza',
    description: 'Umiejętność strzelania z kuszy, zwiększająca precyzję trafień.',
  },
  {
    id: 'mageCircle',
    icon: 'Barbarian_8',
    name: 'Krąg Magii',
    description: 'Poziom umiejętności magicznych, umożliwiający dostęp do potężniejszych zaklęć.',
  },
]

import React from 'react'
import {
  PreparePhaseContainer,
  PreparePhaseHeader,
  PreparePhaseText,
  PreparePhaseWrapper,
  Separator,
} from './styled'

import SeparatorImg from '../../assets/separator.png'

type Props = {
  currentWave: number
  remainingPrepareTime: number
  votesYes: number
  playerCount: number
}

export const PreparePhaseBox = ({
  currentWave,
  remainingPrepareTime,
  votesYes,
  playerCount,
}: Props) => {
  return (
    <PreparePhaseContainer>
      <PreparePhaseWrapper>
        <>
          <PreparePhaseHeader>{'Przetrwano fal:'}</PreparePhaseHeader>
          <PreparePhaseText>{currentWave-1}</PreparePhaseText>
          <Separator src={SeparatorImg} alt="" />

          <PreparePhaseHeader>{'Czas:'}</PreparePhaseHeader>
          <PreparePhaseText>{remainingPrepareTime}</PreparePhaseText>
          <Separator src={SeparatorImg} alt="" />

          <PreparePhaseHeader>{'Pomiń przerwę:'}</PreparePhaseHeader>
          <PreparePhaseText>{`${votesYes}/${playerCount}`}</PreparePhaseText>
          <Separator src={SeparatorImg} alt="" />
        </>
      </PreparePhaseWrapper>
    </PreparePhaseContainer>
  )
}

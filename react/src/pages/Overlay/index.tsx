import React, { useEffect, useState } from 'react'
import { Notification } from '../../components/Notification'
import { Quest, QuestTitle, Quests } from '../../components/Quests'
import HotBar from '../../components/Hotbar'
import { HeroBar } from '../../components/HeroBar'
import { ProgressBar } from '../../components/ProgressBar'
import { sendGameCommand, useGameState } from '../../utils/electron'
import { onGameEvent } from '../../utils/electron'
import { Toaster } from 'react-hot-toast'
import { Item, popUpFailed, popUpSuccess } from '../../utils/item-helpers'
import { PreparePhaseBox } from './PreparePhaseBox'
import { PartyMember, TQuest } from './helpers'
import IconLeafsImg from './assets/icon_leafs_128.png'
import {
  Column,
  ColumnBottomCenter,
  Myself,
  Overlay,
  PartyMemberWrapper,
  Players,
  PlayersWrapper,
  QuestsWrapper,
} from './styled'
import { PlayAudio } from '../../utils/audio'

export type OverlayState = {
  playerId: number
  progressBarLabel: string
  partyMembers: PartyMember[]
  quests: TQuest[]
  remainingPrepareTime: number
  votesYes: number
  playerCount: number
  currentWave: number
  hotbarSlots: (Item | null)[]
}

const OverlayPage = () => {
  const overlayState = useGameState<OverlayState>('OverlayComponent')

  const [isNotificationVisible, setIsNotificationVisible] = useState(false)
  const [notificationTitle, setNotificationTitle] = useState('')
  const [notificationText, setNotificationText] = useState('')

  const [isProgressBarVisible, setIsProgressBarVisible] = useState(false)
  const [progressBarLabel, setProgressBarLabel] = useState('')

  useEffect(() => {
    return onGameEvent('Popup', (data: any) => {
      const { text, status } = data

      status === 'success' ? popUpSuccess(text) : popUpFailed(text)
    })
  }, [])

  useEffect(() => {
    return onGameEvent('PlayAudio', (data: any) => {
      const { fileName } = data
      PlayAudio(fileName)
    })
  }, [])

  useEffect(() => {
    return onGameEvent('Notification', (data: any) => {
      const { title, text, time } = data

      setNotificationTitle(title)
      setNotificationText(text)
      setIsNotificationVisible(true)

      setTimeout(() => {
        setIsNotificationVisible(false)
      }, parseInt(time))
    })
  }, [])

  useEffect(() => {
    return onGameEvent('ProgressBar', (data: any) => {
      const { isVisible, label } = data

      setProgressBarLabel(label)
      setIsProgressBarVisible(isVisible)
    })
  }, [])

  const selfPlayerMember =
    overlayState?.playerId != null && overlayState?.partyMembers
      ? overlayState?.partyMembers?.find((member) => member.id === overlayState.playerId)
      : null

  return (
    <>
      <Overlay>
        <Toaster position="bottom-right"></Toaster>
        <Column>
          {selfPlayerMember && (
            <Myself key={selfPlayerMember.name}>
              <HeroBar
                type={'player'}
                avatar={selfPlayerMember.avatarUrl!}
                name={selfPlayerMember.name!}
                level={selfPlayerMember.level!}
                hp={selfPlayerMember.currentHp!}
                mana={selfPlayerMember.currentMana!}
              />
            </Myself>
          )}
        </Column>
        <Column>
          {isNotificationVisible && (
            <Notification title={notificationTitle} text={notificationText} />
          )}
          <ProgressBar
            icon={IconLeafsImg}
            isVisible={isProgressBarVisible}
            time={5}
            label={progressBarLabel}
            onEnd={() => sendGameCommand('ReviveSuccess')}
          />
          <ColumnBottomCenter>
            <HotBar HotBarItems={overlayState?.hotbarSlots!} />
          </ColumnBottomCenter>
        </Column>
        <Column>
          {overlayState?.currentWave !== 0 &&
            overlayState?.remainingPrepareTime! != null &&
            overlayState?.remainingPrepareTime! > 0 && (
              <PreparePhaseBox
                currentWave={overlayState?.currentWave!}
                remainingPrepareTime={overlayState?.remainingPrepareTime!}
                votesYes={overlayState?.votesYes!}
                playerCount={overlayState?.playerCount!}
              ></PreparePhaseBox>
            )}
          <QuestsWrapper>
            <Quests>
              <QuestTitle title="Aktualne zadania " />
              <div>
                {overlayState?.quests?.map((quest) => (
                  <Quest key={quest.id!} title={quest.title!} status={quest.status!} />
                ))}
              </div>
            </Quests>
          </QuestsWrapper>
          <PlayersWrapper>
            <Players>
              {overlayState?.playerId != null &&
                overlayState?.partyMembers
                  ?.filter((m) => m.id !== overlayState.playerId)
                  .map((member) => (
                    <PartyMemberWrapper key={member.id}>
                      <HeroBar
                        key={member.id}
                        type="friend"
                        avatar={member.avatarUrl!}
                        name={member.name!}
                        level={member.level!}
                        hp={member.currentHp!}
                        mana={member.currentMana!}
                      />
                    </PartyMemberWrapper>
                  ))}
            </Players>
          </PlayersWrapper>
        </Column>
      </Overlay>
    </>
  )
}

export default OverlayPage

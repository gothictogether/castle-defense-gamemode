import styled from 'styled-components'
import { COLOR_DARK_TRANSPARENT, COLOR_LIGHT } from '../../utils/color-constants'

export const Overlay = styled.div`
  display: flex;
  flex-basis: 0;
  width: 100%;
  height: 100%;
`

export const PreparePhaseContainer = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
`

export const PreparePhaseWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: start;
  max-width: 170px;
  height: auto;
  text-align: center;
  background-color: ${COLOR_DARK_TRANSPARENT};
  box-shadow: 0px 0px 10px #000;
  border-radius: 10px;
  padding: 10px;
`
export const PreparePhaseHeader = styled.div`
  font-size: 14px;
  color: ${COLOR_LIGHT};
  padding: 5px;
  margin-left: 5px;
`
export const PreparePhaseText = styled.div`
  font-size: 12px;
  margin: 2px;
  color: ${COLOR_LIGHT};
  padding: 5px;
`

export const Column = styled.div`
  position: relative;
  flex-basis: inherit;
  flex-grow: 1;
`

export const ColumnBottomCenter = styled.div`
  display: flex;
  justify-content: center;
`

export const QuestsWrapper = styled.div`
  margin-top: 200px;
  margin-bottom: 30px;
  display: flex;
  flex-direction: column;
  align-items: end;
`

export const PlayersWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
  align-items: end;
`

export const Players = styled.div`
  margin-right: -120px;
`

export const PartyMemberWrapper = styled.div`
  width: 400px;
  height: 80px;
`

export const Myself = styled.div`
  position: absolute;
  bottom: 0;
  left: 25px;
`
export const Separator = styled.img``

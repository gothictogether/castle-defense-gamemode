import { QuestStatus } from '../../components/Quests'

export type PartyMember = {
  id?: number
  name?: string
  avatarUrl?: string
  level?: number
  currentHp?: number
  currentMana?: number
}

export type TQuest = {
  id?: string
  title?: string
  status?: QuestStatus
}

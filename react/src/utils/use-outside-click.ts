import { RefObject, useEffect } from 'react'

export function useOutsideClick(ref: RefObject<HTMLElement>, onClickOut: () => void, deps = []) {
  useEffect(() => {
    const onClick = ({ target }: MouseEvent) => {
      if (!ref.current?.contains(target as Node)) {
        onClickOut?.()
      }
    }
    document.addEventListener('click', onClick)

    return () => {
      document.removeEventListener('click', onClick)
    }
  }, deps)
}

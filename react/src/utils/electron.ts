import { useEffect, useState } from 'react'

export const onGameEvent = (name: string, func: (data: any) => void): (() => void) => {
  return window.electron.events.on(name, (stringJson: string) => {
    console.log(`>> ${name}: ${stringJson}`)
    func(JSON.parse(stringJson))
  })
}

export const sendGameCommand = (name: string, args: any = {}) => {
  console.log(`<< ${name}: ${JSON.stringify(args)}`)
  window.electron.commands.send(name, args)
}

export const useGameState = <T>(name: string) => {
  const [gameState, setGameState] = useState<T>()

  useEffect(() => {
    let currentState = gameState
    return onGameEvent(`STATE_${name}`, (data: T) => {
      currentState = Object.assign({}, currentState || {}, data)
      setGameState(currentState)
    })
  }, [gameState])

  useEffect(() => {
    sendGameCommand(`RequestState${name}`)
  }, [])

  return gameState
}

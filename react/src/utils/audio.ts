export const PlayAudio = (fileName: string) => {
  new Audio(`/audio/${fileName}`).play()
}

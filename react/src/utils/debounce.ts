export const debounce = (mainFunction: (...args: any[]) => void, delay: number) => {
    let timer: NodeJS.Timeout
    return function (...args: any[]) {
      clearTimeout(timer)
      timer = setTimeout(() => {
        mainFunction(...args)
      }, delay)
    }
  }
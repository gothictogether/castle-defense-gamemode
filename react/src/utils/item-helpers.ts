import toast from 'react-hot-toast'
import { COLOR_DARK_TRANSPARENT, COLOR_LIGHT } from './color-constants'
import itemsJson from '../assets/items.json'

export type JsonItem = {
  fileName: string
  objectName: string
  price: number
  name: string
  totalDamage: number
  requiredAttribute: number
  requiredAttributeValue: number
  description1: string
  value1: number
  description2: string
  value2: number
  description3: string
  value3: number
  description4: string
  value4: number
  description5: string
  value5: number
}

export type Item = {
  objectName: string
  category?: string
  amount: number
  isNative: boolean
  isEquipped: boolean
}

export const objectNameToJsonItemMap = new Map<string, JsonItem>()
itemsJson.forEach((item) => {
  if (item.objectName) {
    objectNameToJsonItemMap.set(item.objectName, item)
  }
})

export const getWeaponType = (fileName: string) => {
  if (fileName.includes('1H')) return 'Broń jednoręczna'
  else if (fileName.includes('2H')) return 'Broń dwuręczna'
  else if (fileName.includes('CROSS')) return 'Kusza'
  else if (fileName.includes('BOW')) return 'Łuk'
  else return ''
}

export const popUpSuccess = (text: string) => {
  toast.success(`${text}`, {
    style: {
      background: COLOR_DARK_TRANSPARENT,
      color: COLOR_LIGHT,
    },
  })
}

export const popUpFailed = (text: string) => {
  toast.error(`${text}`, {
    style: {
      background: COLOR_DARK_TRANSPARENT,
      color: COLOR_LIGHT,
    },
  })
}

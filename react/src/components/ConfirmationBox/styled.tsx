import styled from 'styled-components'
import { COLOR_LIGHT } from '../../utils/color-constants'
import ItemSlotImg from '../../assets/item-slot.png'

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 24rem;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const CloseButton = styled.img`
  margin: 5px;
  width: 40px;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: end;
  align-content: center;
  flex-wrap: wrap;
  width: 560px;
  max-height: 10px;
`
export const Container = styled.div`
  z-index: 100;
  height: 100%;
  width: 100%;
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const ConfirmationBoxWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 560px;
  height: 140px;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.9);
  box-shadow: 0px 0px 20px #000;
  border-radius: 5px;
  padding: 10px;
  margin: 20px;
`

export const ConfirmationBoxHeader = styled.h1`
  text-transform: uppercase;
  font-size: 20px;
  text-shadow: 0.5px 0.866025px 0 #000000;
`

export const ConfirmationBoxText = styled.h2`
  margin-top: 10px;
  text-align: center;
  text-shadow: 0.5px 0.866025px 0 #000000;
  line-height: 24px;
  width: 560px;
  margin-bottom: 10px;
  word-wrap:;
`

export const ButtonWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 40px;
  width: 120px;
  align-items: center;
  justify-content: space-around;
`
export const Button = styled.button`
  margin: 5px;
  padding: 3px;
  height: 20px;
  width: 40px;
  background-color: gray;
  background-image: url(${ItemSlotImg});
  text-align: center;
  color: ${COLOR_LIGHT};
  font-size: 12px;
  cursor: pointer;
`

import React from 'react'
import {
  ButtonWrapper,
  CloseButton,
  ConfirmationBoxHeader,
  ConfirmationBoxText,
  ConfirmationBoxWrapper,
  Container,
  HeaderContainer,
  Overlay,
  Button,
} from './styled'
import CloseBtnImg from '../../assets/close.png'

export const ConfirmationBox = ({
  title,
  text,
  onClose,
  onYes,
  onNo,
}: {
  title: string
  text: string
  onClose: () => void
  onYes: () => void
  onNo: () => void
}) => {
  return (
    <Overlay>
      <Container>
        <ConfirmationBoxWrapper>
          <HeaderContainer>
            <CloseButton src={CloseBtnImg} alt="" onClick={onClose} />
          </HeaderContainer>
          <ConfirmationBoxHeader>{title}</ConfirmationBoxHeader>
          <ConfirmationBoxText>{text}</ConfirmationBoxText>
          <ButtonWrapper>
            <Button onClick={onYes}>{'TAK'}</Button>
            <Button onClick={onNo}>{'NIE'}</Button>
          </ButtonWrapper>
        </ConfirmationBoxWrapper>
      </Container>
    </Overlay>
  )
}

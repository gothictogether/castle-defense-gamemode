import styled from 'styled-components'

export const BarBackground = styled.img`
  width: 381px;
  height: 117px;
`

export const AvatarWrapper = styled.div`
  position: absolute;
  display: block;
  width: 67px;
  height: 67px;
  border: 1px solid #000;
  transform: rotate(45deg);
  overflow: hidden;
  top: 26px;
  left: 25px;
`

export const Avatar = styled.img<{ $isalive: boolean }>`
  width: 145%;
  max-width: 145%;
  transform: rotate(-45deg);
  margin: -14px;
  filter: ${({ $isalive }) => ($isalive ? 'none' : 'saturate(0)')};
`;

export const HeroWrapper = styled.div`
  position: relative;
`

export const LevelText = styled.div`
  position: absolute;
  top: 76px;
  left: 26px;
  height: 20px;
  width: 20px;
  font-size: 14px;
  text-align: center;
`

export const LevelBackground = styled.img`
  position: absolute;
  top: 65px;
  left: 15px;
`

export const HpBarWrapper = styled.div`
  position: absolute;
  top: 29px;
  left: 81px;
  overflow: hidden;
`

export const HpBar = styled.img`
  width: 289px;
  height: 35px;
  max-width: unset;
`

export const ManaBarWrapper = styled.div`
  position: absolute;
  top: 70px;
  left: 80px;
  overflow: hidden;
`

export const ManaBar = styled.img`
  width: 271px;
  height: 20px;
  max-width: unset;
`

export const HeroName = styled.div`
  text-transform: uppercase;
  position: absolute;
  top: 2px;
  left: 80px;
  font-size: 18px;
  text-shadow:
    1px 1px 2px rgb(187 181 172),
    0 0 1px rgb(187 181 163),
    0 0 0.2px rgb(187 181 172);
`

import React from 'react'
import {
  Avatar,
  AvatarWrapper,
  BarBackground,
  HeroName,
  HeroWrapper,
  HpBar,
  HpBarWrapper,
  LevelBackground,
  LevelText,
  ManaBar,
  ManaBarWrapper,
} from './styled'

import HeroBarImg from './assets/herobar.png'
import LevelImg from './assets/level.png'
import HpFillImg from './assets/hp-fill.png'
import ManaFillImg from './assets/mana-fill.png'

type Props = {
  avatar: string
  level: number
  name: string
  hp: number
  mana: number
  type: 'player' | 'friend'
}

const MAX_HP_BAR_WIDTH = 289
const MAX_MANA_BAR_WIDTH = 271

export const HeroBar = ({ type = 'player', avatar, level, name, hp, mana }: Props) => {
  return (
    <HeroWrapper
      style={type === 'friend' ? { transform: 'scale(0.65)', transformOrigin: 'top left' } : {}}
    >
      <BarBackground src={HeroBarImg} alt="" />
      <AvatarWrapper>
        <Avatar src={avatar} alt="" $isalive={hp > 0} />
      </AvatarWrapper>
      <LevelBackground src={LevelImg} alt="" />
      <LevelText>{level}</LevelText>

      <HeroName>{name}</HeroName>

      <HpBarWrapper style={{ width: (hp / 100) * MAX_HP_BAR_WIDTH }}>
        <HpBar src={HpFillImg} alt="" />
      </HpBarWrapper>
      <ManaBarWrapper style={{ width: (mana / 100) * MAX_MANA_BAR_WIDTH }}>
        <ManaBar src={ManaFillImg} alt="" />
      </ManaBarWrapper>
    </HeroWrapper>
  )
}

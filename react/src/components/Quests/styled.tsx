import styled from 'styled-components'

export const QuestsWrapper = styled.div`
  max-width: 250px;
  margin: 20px;
  display: flex;
  flex-direction: column;
  background-color: #000000bf;
  box-shadow: 0px 0px 10px #000;
  border-radius: 10px;
  padding: 10px;
`

export const Title = styled.h2`
  font-size: 18px;
  text-transform: uppercase;
  color: #cac2b8;
  text-shadow: 0.5px 0.866025px 0 #000000;
`

export const QuestTitleText = styled.h2`
  color: #e4dfd7;
  font-size: 15px;
  text-shadow: 0.5px 0.866025px 0 #000000;
  margin-top: 5px;
`

export const QuestText = styled.p`
  text-shadow: 0.5px 0.866025px 0 #000000;
  font-size: 14px;
`

export const Separator = styled.img``

export const QuestList = styled.div``

export const QuestWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-left: 10px;
`

export const QuestCheckbox = styled.img`
  height: 12px;
  width: 12px;
  position: absolute;
  top: 0;
  left: 0;
`

export const QuestCheckboxStatus = styled.img`
  height: 8px;
  width: 8px;
  position: absolute;
  top: 2px;
  left: 2px;
`

export const CheckboxWrapper = styled.div`
  position: relative;
  margin-right: 3px;
  height: 12px;
  width: 12px;
`

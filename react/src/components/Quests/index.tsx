import React from 'react'
import {
  CheckboxWrapper,
  QuestCheckbox,
  QuestCheckboxStatus,
  QuestList,
  QuestsWrapper,
  QuestText,
  QuestTitleText,
  QuestWrapper,
  Separator,
  Title,
} from './styled'

import BarImg from './assets/bar.png'
import CheckEmptyImg from './assets/check-empty.jpg'
import CheckComplete from './assets/check-complete.png'
import CheckFailedImg from './assets/check-failed.png'

export type QuestStatus = 'inprogress' | 'failed' | 'done'

export const Quests = ({ children }: { children: JSX.Element[] }) => {
  return (
    <QuestsWrapper>
      <Title>Zadania</Title>
      <Separator src={BarImg} alt="" />
      <QuestList>{children}</QuestList>
    </QuestsWrapper>
  )
}

export const QuestTitle = ({ title }: { title: string }) => {
  return <QuestTitleText>{title}</QuestTitleText>
}

export const Quest = ({
  title,
  status = 'inprogress',
}: {
  title: string
  status?: QuestStatus
}) => {
  return (
    <QuestWrapper>
      <CheckboxWrapper>
        <QuestCheckbox src={CheckEmptyImg} alt="" />
        {status === 'done' && <QuestCheckboxStatus src={CheckComplete} alt="" />}
        {status === 'failed' && <QuestCheckboxStatus src={CheckFailedImg} alt="" />}
      </CheckboxWrapper>
      <QuestText>{title}</QuestText>
    </QuestWrapper>
  )
}

import styled from 'styled-components'
import { COLOR_DARK_TRANSPARENT } from '../../utils/color-constants'

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  pointer-events: none;
`

export const Container = styled.div`
  z-index: 100;
  height: 100%;
  width: 100%;
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;
  align-items: center;
  justify-content: center;
  animation: fadeIn 0.5s linear forwards;
  opacity: 0;

  @keyframes fadeIn {
    0% { opacity: 0; }
    100% { opacity: 1; }

`

export const NotificationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 650px;
  text-align: center;
  background-color: ${COLOR_DARK_TRANSPARENT};
  box-shadow: 0px 0px 20px #000;
  border-radius: 10px;
  padding: 10px;
  margin: 20px;
`

export const Separator = styled.img``

export const NotificationHeader = styled.h1`
  text-transform: uppercase;
  font-size: 40px;
  text-shadow: 0.5px 0.866025px 0 #000000;
`

export const NotificationText = styled.h2`
  margin-top: 10px;
  text-align: center;
  text-shadow: 0.5px 0.866025px 0 #000000;
  line-height: 24px;
  width: 550px;
  margin-bottom: 10px;
`

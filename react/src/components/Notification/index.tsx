import React from 'react'
import {
  Container,
  NotificationHeader,
  NotificationText,
  NotificationWrapper,
  Overlay,
  Separator,
} from './styled'

import SeparatorImg from '../../assets/separator.png'

const addLineBreak = (str: string) =>
  str.split('\n').map((subStr) => {
    return (
      <>
        {subStr}
        <br />
      </>
    )
  })

export const Notification = ({ title, text }: { title: string; text: string }) => {
  return (
    <Overlay>
      <Container>
        <NotificationWrapper>
          <NotificationHeader>{title}</NotificationHeader>
          <Separator src={SeparatorImg} alt="" />
          <NotificationText>{addLineBreak(text)}</NotificationText>
        </NotificationWrapper>
      </Container>
    </Overlay>
  )
}

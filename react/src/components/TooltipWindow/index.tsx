import React from 'react'
import {
  InventoryTooltip,
  InventoryTooltipAmount,
  InventoryTooltipCost,
  InventoryTooltipFooter,
  InventoryTooltipTitle,
  TooltipContainer,
} from './styled'
import { JsonItem } from '../../utils/item-helpers'

type Props = {
  item: JsonItem
  content?: JSX.Element
  amount?: number
  iconImage: JSX.Element
}

export const TooltipWindow = ({ item, content, amount, iconImage }: Props) => {
  return (
    <TooltipContainer>
      <InventoryTooltip>
        <InventoryTooltipTitle>{item.name}</InventoryTooltipTitle>
        {content}
        <InventoryTooltipFooter>
          {amount && <InventoryTooltipAmount>x {amount}</InventoryTooltipAmount>}
          <InventoryTooltipCost>
            {item.price}
            {iconImage}
          </InventoryTooltipCost>
        </InventoryTooltipFooter>
      </InventoryTooltip>
    </TooltipContainer>
  )
}

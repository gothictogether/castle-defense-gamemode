import styled from 'styled-components'
import { COLOR_DARK_TRANSPARENT, COLOR_DARK_YELLOW_TRANSPARENT } from '../../utils/color-constants'

export const TooltipContainer = styled.div`
  width: 300px;
  height: auto;
`

export const InventoryTooltip = styled.div`
  background-color: ${COLOR_DARK_TRANSPARENT};
  width: 100%;
  max-height: 100%;
  height: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  box-shadow: -4px 25px 20px -8px ${COLOR_DARK_TRANSPARENT};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
`

export const InventoryTooltipTitle = styled.div`
  background-color: ${COLOR_DARK_YELLOW_TRANSPARENT};
  text-align: center;
  width: 99%;
  margin: 0;
  padding: 5px 0;
`

export const InventoryTooltipFooter = styled.div`
  margin-top: 10px;
  display: flex;
  width: 100%;
  justify-content: space-between;
  padding-left: 15px;
  padding-right: 10px;
`

export const InventoryTooltipAmount = styled.div``
export const InventoryTooltipCost = styled.div`
  display: flex;
`

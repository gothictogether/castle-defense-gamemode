import styled from 'styled-components'
import { COLOR_LIGHT } from '../../utils/color-constants'

export const Container = styled.div`
  position: absolute;
  z-index: 100;
  height: 100%;
  width: 100%;
  bottom: 75px;
  display: flex;
  align-items: center;
`
export const BarBackground = styled.img`
  width: 512px;
  height: 49px;
`

export const IconWrapper = styled.div`
  position: absolute;
  display: block;
  width: 38px;
  height: 37px;
  border: 1px solid #000;
  transform: rotate(45deg);
  overflow: hidden;
  top: 10px;
  left: 7px;
`

export const Icon = styled.img`
  max-width: 160%;
  transform: rotate(-45deg);
  margin: -14px;
`

export const BarWrapper = styled.div`
  position: relative;
  top: 24vw;
  width: 100%;
  margin-left: 20%;
`

export const FillingBarWrapper = styled.div`
  position: absolute;
  top: 11px;
  left: 36px;
  width: 100%;
`

export const FillingBar = styled.img<{ time: number }>`
  height: 26px;
  width: 0;
  animation-name: progressBar;
  animation-timing-function: linear;
  animation-duration: ${(props) => props.time}s;
  @keyframes progressBar {
    0% {
      width: 0%;
    }

    100% {
      width: 470px;
    }
  }
`

export const FillingBarLabel = styled.div<{ label: string }>`
  color: ${COLOR_LIGHT};
  font-size: 20px;
  position: absolute;
  left: 20px;
  top: -1px;
  font-weight: bold;
`

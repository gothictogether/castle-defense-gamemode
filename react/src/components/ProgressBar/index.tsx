import React from 'react'
import {
  BarBackground,
  BarWrapper,
  Container,
  FillingBar,
  FillingBarLabel,
  FillingBarWrapper,
  Icon,
  IconWrapper,
} from './styled'

import BarImg from './assets/bar.png'
import FillingImg from './assets/filling.png'

type Props = {
  isVisible: boolean
  time: number
  icon: string
  label: string
  onEnd: () => void
}

export const ProgressBar = ({ isVisible, time, icon, label, onEnd }: Props) => {
  if (!isVisible) {
    return null
  }

  return (
    <Container>
      <BarWrapper>
        <BarBackground src={BarImg} alt="" />
        <FillingBarWrapper>
          <FillingBarLabel label={label}>{label}</FillingBarLabel>
          <FillingBar
            time={time}
            src={FillingImg}
            alt=""
            onAnimationEnd={onEnd}
          />
        </FillingBarWrapper>
        <IconWrapper>
          <Icon src={icon} alt="" />
        </IconWrapper>
      </BarWrapper>
    </Container>
  )
}

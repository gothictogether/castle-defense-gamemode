import styled from 'styled-components'
import { COLOR_DARK_TRANSPARENT, COLOR_GOLD, COLOR_LIGHT } from '../../utils/color-constants'

export const Wrapper = styled.div`
  position: absolute;
  bottom: 0;
  display: flex;
`
export const Box = styled.div`
  position: relative;
  width: 70px;
  height: 70px;
  margin: 1px;
`

export const Border = styled.img`
  background-color: ${COLOR_DARK_TRANSPARENT};
  z-index: 1;
  filter: brightness(0.5);
`

export const ItemImage = styled.img`
  padding: 8px;
  top: 0px;
  position: absolute;
  z-index: 2;
  filter: grayscale(0.50);
`

export const Badge = styled.div`
  position: absolute;
  top: 8px;
  left: 12px;
  color: ${COLOR_LIGHT};
  font-size: 0.8rem;
  height: 15px;
  width: 40px;
  z-index: 3;
`

export const BadgeAmount = styled.div`
  position: absolute;
  color: ${COLOR_GOLD};
  bottom: 8px;
  right: 12px;
  font-size: 0.8rem;
  height: 15px;
  width: 40px;
  text-align: right;
  font-weight: 700;
  z-index: 3;
`

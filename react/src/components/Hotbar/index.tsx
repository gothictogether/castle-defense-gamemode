import React from 'react'
import { boxNumbers, GetItemFileName } from './helpers'
import { Badge, BadgeAmount, Border, Box, ItemImage, Wrapper } from './styled'
import { Item } from '../../utils/item-helpers'
import MainSlotBorderImg from './assets/MainSlotBorder.png'

type Props = {
  HotBarItems: (Item | null)[]
}

const HotBar = ({ HotBarItems }: Props) => {
  if (!HotBarItems){
    return null
  }
  
  return (
    <>
      <Wrapper>
        {boxNumbers.map((num) => (
          <Box key={num}>
            <Border src={MainSlotBorderImg}/>
            {HotBarItems[num - 1] && (
              <ItemImage
                src={`/vob-images/${GetItemFileName(HotBarItems[num - 1]?.objectName!)}.png`}
              />
            )}
            <Badge>{num}</Badge>
            {HotBarItems[num - 1]?.amount! > 1 && (
              <BadgeAmount>{HotBarItems[num - 1]?.amount}</BadgeAmount>
            )}
          </Box>
        ))}
      </Wrapper>
    </>
  )
}

export default HotBar

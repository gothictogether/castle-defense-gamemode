import { objectNameToJsonItemMap } from '../../utils/item-helpers'

export const GetItemFileName = (objectName: string) => {
  return objectNameToJsonItemMap?.get(objectName)?.fileName
}

export const boxNumbers = Array.from({ length: 9 }, (_, i) => i + 1)

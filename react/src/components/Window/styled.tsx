import styled from 'styled-components'
import { COLOR_LIGHT } from '../../utils/color-constants'

export const WindowWrapper = styled.div``

export const WindowBody = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`

export const BorderTop = styled.img`
  width: 100%;
  margin-left: 7px;
  padding-right: 13px;
`

export const BorderBottom = styled.img`
  width: 100%;
  margin-left: 7px;
  padding-right: 13px;
`

export const BorderLeft = styled.img`
  height: 100%;
`

export const BorderRight = styled.img`
  height: 100%;
`

export const WindowBodyWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`

export const WindowTitle = styled.div`
  height: 50px;
  background-color: #000000ab;
  text-align: center;
  color: ${COLOR_LIGHT};
  text-transform: uppercase;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 24px;
  margin-top: -3.5px;
  margin-bottom: -3.5px;
  flex-grow: 1;
  width: 33%;
`

export const WindowContent = styled.div<{ $isscrollable: boolean }>`
  height: 100%;
  background-color: #000c;
  padding: 10px 20px;
  overflow-y: ${(props) => (props.$isscrollable ? 'scroll' : 'hidden')};
  &::-webkit-scrollbar {
    display: ${(props) => (props.$isscrollable ? 'none' : 'block')};
  }
`

export const TitleHrLine = styled.img``

export const BottomHrLine = styled.img`
  margin-top: -3.5px;
  margin-left: 7px;
  padding-right: 13px;
`

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export const LeftAction = styled.div`
  background-color: #000000ab;
  height: 50px;
  flex-grow: 1;
  margin-top: -3.5px;
  margin-bottom: -3.5px;
  width: 33%;
`

export const RightAction = styled.div`
  background-color: #000000ab;
  height: 50px;
  flex-grow: 1;
  margin-top: -3.5px;
  margin-bottom: -3.5px;
  display: flex;
  justify-content: end;
  width: 33%;
`

export const CloseButton = styled.img`
  margin: 5px;
`

import React from 'react'
import { sendGameCommand } from '../../utils/electron'
import {
  BorderBottom,
  BorderLeft,
  BorderRight,
  BorderTop,
  BottomHrLine,
  CloseButton,
  LeftAction,
  RightAction,
  TitleHrLine,
  TitleWrapper,
  WindowBody,
  WindowBodyWrapper,
  WindowContent,
  WindowTitle,
  WindowWrapper,
} from './styled'

import BorderTopImg from './assets/border-top.png'
import BorderLeftImg from './assets/border-left.png'
import BorderRightImg from './assets/border-right.png'
import BorderBottomImg from './assets/border-bottom.png'

import HrTitleTopImg from './assets/hr-title-top.png'
import HrTitleBottomImg from './assets/hr-title-bottom.png'

import CloseBtnImg from '../../assets/close.png'

type Props = {
  title: string
  children: JSX.Element | JSX.Element[]
  leftContent?: JSX.Element
  rightContent?: JSX.Element
  width?: number
  height?: number
  scrollableContent: boolean
}

export const Window = ({
  children,
  title,
  leftContent,
  rightContent,
  width = 500,
  height = 500,
  scrollableContent = true,
}: Props) => {
  return (
    <WindowWrapper style={{ width: width, height: height }}>
      <BorderTop src={BorderTopImg} alt="" />
      <WindowBodyWrapper>
        <BorderLeft src={BorderLeftImg} alt="" />
        <WindowBody>
          <TitleHrLine src={HrTitleTopImg} alt="" />
          <TitleWrapper>
            <LeftAction>{leftContent}</LeftAction>
            <WindowTitle>{title}</WindowTitle>
            <RightAction>
              {rightContent}
              <CloseButton
                src={CloseBtnImg}
                alt=""
                onClick={() => {
                  sendGameCommand('StopIntercept')
                }}
              />
            </RightAction>
          </TitleWrapper>
          <TitleHrLine src={HrTitleBottomImg} alt="" />
          <WindowContent $isscrollable={scrollableContent}>{children}</WindowContent>
        </WindowBody>
        <BorderRight src={BorderRightImg} alt="" />
      </WindowBodyWrapper>
      <BottomHrLine src={HrTitleBottomImg} alt="" />
      <BorderBottom src={BorderBottomImg} alt="" />
    </WindowWrapper>
  )
}

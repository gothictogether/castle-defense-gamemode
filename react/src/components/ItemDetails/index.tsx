import React from 'react'
import { ItemDetail, ItemDetailsWrapper, ItemDetailText, ItemDetailValue } from './styled'
import { getWeaponType, JsonItem } from '../../utils/item-helpers'

export const ItemDescription = ({ item }: { item: JsonItem }) => {
  return (
    <ItemDetailsWrapper>
      <ItemDetails text={item.description1} value={item.value1} />
      <ItemDetails text={item.description2} value={item.value2} />
      <ItemDetails text={item.description3} value={item.value3} />
      <ItemDetails text={item.description4} value={item.value4} />
      <ItemDetails text={item.description5} value={item.value5} />
      <ItemDetails text={getWeaponType(item.objectName)} value={-1}></ItemDetails>
    </ItemDetailsWrapper>
  )
}

export const ItemDetails = ({ text, value }: { text: string; value: number }) => {
  if (!text || value === 0) {
    return null
  }

  return (
    <ItemDetail>
      <ItemDetailText>{text}</ItemDetailText>
      <ItemDetailValue>{value > 0 && value}</ItemDetailValue>
    </ItemDetail>
  )
}

import styled from 'styled-components'

export const ItemDetail = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 0 15px;
`

export const ItemDetailText = styled.div``
export const ItemDetailValue = styled.div``

export const ItemDetailsWrapper = styled.div`
  margin-top: 10px;
  width: 100%;
`

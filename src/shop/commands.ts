import { MyPlayer as Player } from 'src/player.js'
import { GameMode, REACT_BASE_URL } from 'src/gamemode.js'
import { Client } from 'gothic-together'
import { AddItemToInventory, GetPlayerGold, RemoveItemFromInventory } from 'src/inventory/index.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'
import { ShopState } from 'react/src/pages/Shop/index.js'
import { GetItem, GetPurchaseAmount } from './utils.js'
import { ShowPopup } from 'src/utils/notifications.js'
import { CreateItem } from 'src/inventory/item.js'
import { OverlayState } from 'react/src/pages/Overlay/index.js'

const Shop = (player: Player, args: any, gameMode: GameMode) => {
  Client.NavigateHtmlComponent(player.Id, 'Main', `${REACT_BASE_URL}#shop`)
  UpdatePlayerGoldState(player)
}

const UpdatePlayerGoldState = (player: Player) => {
  const gold = GetPlayerGold(player)

  UpdateOverlayState<ShopState>(player, 'ShopComponent', {
    playerGold: gold,
  })
}

const PurchaseItem = (player: Player, objectName: string) => {
  const item = GetItem(objectName)

  if (!item) {
    return
  }

  const gold = GetPlayerGold(player)

  if (gold >= item.price) {
    RemoveItemFromInventory(player, 'ITMI_GOLD', item.price)
    AddItemToInventory(player, CreateItem(item.objectName, GetPurchaseAmount(item.objectName)))

    UpdatePlayerGoldState(player)

    UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
      hotbarSlots: player.Attrs.hotbarSlots,
    })

    ShowPopup(player, `Kupiłeś przedmiot ${item.name}`, 'success')
  } else {
    ShowPopup(player, `Masz zbyt mało złota na przedmiot ${item.name}`, 'failed')
  }
}

export default {
  Shop,
  PurchaseItem,
}

import itemsJson from 'react/src/assets/items.json'

export const GetItem = (objectName: string) => {
  const item = itemsJson.find((item) => item.objectName === objectName)

  return item
}

export const GetPurchaseAmount = (objectName: string) => {
  const amount = objectName == 'ITRW_BOLT' || objectName == 'ITRW_ARROW' ? 100 : 1

  return amount
}

import {
  NPC_ATR_DEXTERITY,
  NPC_ATR_HITPOINTS,
  NPC_ATR_HITPOINTSMAX,
  NPC_ATR_MANAMAX,
  NPC_ATR_STRENGTH,
  NPC_WEAPON_NONE,
  oTEnumNpcTalent,
} from 'gothic-together/union/enums'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { GameMode } from 'src/gamemode.js'
import { UpdatePartyMemberOverlay, UpdateQuestOverlay } from 'src/overlay/updater.js'
import { UpdateOverlayState } from './update-overlay-state.js'
import { attributes, InitializePlayerAttributes } from 'src/gamemode/attributes.js'
import { HeroPageState } from 'react/src/pages/Hero/index.js'
import { InitPlayerInventory, UnequipAllItems } from 'src/inventory/index.js'
import { Server } from 'gothic-together'
import { RemoveEnemyCorpsesFromWorld } from 'src/gamemode/wave-handler.js'
import { AppState } from 'react/src/pages/Home/index.js'
import { EquipStartItems } from 'src/gamemode/inventory.js'

export const RestartGamemode = (gameMode: GameMode) => {
  gameMode.state.setGamePhase('INIT')
  gameMode.state.restartWaves()
  gameMode.state.restartCurrentWaveEnemiesKilled()
  gameMode.ClearCurrentWaveEnamies()
  gameMode.state.clearCurrentWaveEnemies()
  gameMode.state.setWaveEnemyCount(gameMode.state.currentBaseWaveEnemyCount)
  gameMode.state.restartWaveEnemyStage()
  gameMode.state.restartWaveEnemyIndex()
  gameMode.state.restartPossiblePlayerToAttack()
  gameMode.state.restartWaveEnemiesWithoutAttackFocus()
  gameMode.state.setTimeLeftToEndPreparePhase(0)
  gameMode.state.resetVotesForPreparePhaseSkip()
  gameMode.state.resetCurrentWaveEnemyTypeIndex()

  RemoveEnemyCorpsesFromWorld(gameMode.state)
  gameMode.state.disposeWaveEnemyCorpses()

  for (const p of gameMode.Players) {
    p.Npc.SetAttribute(NPC_ATR_HITPOINTS, p.Npc.GetAttribute(NPC_ATR_HITPOINTSMAX)!)
    p.SetAttrs({ totalWavesSurvived: 0, totalDamageDealt: 0, waveDamageDealt: 0 })

    p.Npc.SetWeaponMode(NPC_WEAPON_NONE)
    p.Npc.DropAllInHand()
    UnequipAllItems(p)

    p.Npc.inventory2()?.ClearInventory()
    InitPlayerInventory(p)

    InitializePlayerAttributes(p, attributes)
    EquipStartItems(p)

    p.SetAttrs({ learningPoints: 0 })

    UpdateOverlayState<HeroPageState>(p, 'HeroComponent', {
      lp: p.Attrs.learningPoints,
      hitpoints: p.Npc.GetAttribute(NPC_ATR_HITPOINTSMAX)!,
      mana: p.Npc.GetAttribute(NPC_ATR_MANAMAX)!,
      strength: p.Npc.GetAttribute(NPC_ATR_STRENGTH)!,
      dexterity: p.Npc.GetAttribute(NPC_ATR_DEXTERITY)!,
      '1h': p.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_1H)!,
      '2h': p.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_2H)!,
      bow: p.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_BOW)!,
      cbow: p.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_CROSSBOW)!,
      mageCircle: p.Npc.GetAttribute(oTEnumNpcTalent.NPC_TAL_MAGE)!,
    })

    UpdateOverlayState<OverlayState>(p, 'OverlayComponent', {
      playerId: p.Id,
      partyMembers: [],
      remainingPrepareTime: 0,
      votesYes: 0,
      playerCount: gameMode.Players.length,
      currentWave: 0,
    })

    UpdateOverlayState<AppState>(p, 'HomePage', {
      currentWave: gameMode.state.waveCounter,
    })

    UpdateQuestOverlay(gameMode, p)
  }

  const garondNpc = gameMode.state.captainGarond

  if (garondNpc) {
    const garondWeapon = Server.PutInInventory('ITMW_1H_MISC_SWORD', garondNpc, 1)

    garondNpc.SetAttribute(NPC_ATR_HITPOINTS, garondNpc.GetAttribute(NPC_ATR_HITPOINTSMAX)!)
    garondNpc.Equip(garondWeapon!)
  }

  gameMode.RoutineManager.RestartRoutines()
  UpdatePartyMemberOverlay(gameMode)
}

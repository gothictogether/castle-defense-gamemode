import { Client } from 'gothic-together'
import { MyPlayer as Player } from 'src/player.js'

export const PlayAudio = (players: Player | Player[], fileName: string) => {
  const playersArray = Array.isArray(players) ? players : [players]

  for (const p of playersArray) {
    Client.SendEventToHtmlComponents(p.Id, 'PlayAudio', {
      fileName: fileName,
    })
  }
}

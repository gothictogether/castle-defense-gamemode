import fs from 'fs'
import superjson, { SuperJSON } from 'superjson'
import { oCNpc } from 'gothic-together/union/classes/index'
import { createStore } from 'zustand/vanilla'
import { persist, PersistStorage } from 'zustand/middleware'
import { BaseUnionObject } from 'gothic-together/union/base-union-object'
import { JSONObject } from 'node_modules/superjson/dist/types.js'
import * as UnionClasses from 'gothic-together/union/classes/index'
import { PlayerAttributes } from './player.js'
import { BASE_WAVE_ENEMY_COUNT, PREPARE_PHASE_TIME } from './gamemode/difficulty.js'
import { BASE_ENEMY_SPAWN_LIMIT } from './gamemode/difficulty.js'
import { Player } from 'gothic-together/player'

type TGamePhase = 'INIT' | 'PREPARE' | 'ACTIVE' | 'END'

export interface GameState {
  readonly players: Record<number, PlayerAttributes>
  initPlayerAttributes: (playerId: number) => void
  deletePlayerAttributes: (playerId: number) => void
  updatePlayerAttributes: (playerId: number, attrs: Partial<PlayerAttributes>) => void

  readonly gamePhase: TGamePhase
  setGamePhase: (value: TGamePhase) => void

  readonly waveCounter: number
  nextWave: () => void
  restartWaves: () => void

  readonly lastWaveEndTime: Date | null
  setLastWaveEndTime: (value: Date) => void
  setLastWaveEndTimeToZero: () => void

  readonly waveEnemySpawnLimit: number
  readonly setWaveEnemySpawnLimit: (value: number) => void

  readonly timeLeftToEndPreparePhase: number
  decrementTimeLeftToEndPreparePhase: () => void
  setTimeLeftToEndPreparePhase: (value: number) => void
  restartTimeLeftToEndPreparePhase: () => void

  readonly currentWaveEnemyType: string
  setCurrentWaveEnemyType: (enemy: string) => void

  readonly currentWaveEnemyTypeIndex: number
  incrementCurrentWaveEnemyTypeIndex: () => void
  resetCurrentWaveEnemyTypeIndex: () => void

  readonly waveEnemyStage: number
  nextWaveEnemyStage: () => void
  restartWaveEnemyStage: () => void

  readonly waveEnemyIndex: number
  nextWaveEnemyIndex: () => void
  restartWaveEnemyIndex: () => void

  readonly waveEnemyCount: number
  setWaveEnemyCount: (value: number) => void
  addWaveEnemyCount: () => void

  readonly currentBaseWaveEnemyCount: number
  setCurrentBaseWaveEnemyCount: (value: number) => void

  readonly possiblePlayersToAttack: oCNpc[]
  addPossiblePlayerToAttack: (npc: oCNpc) => void
  restartPossiblePlayerToAttack: () => void
  removePossiblePlayerToAttack: (npc: oCNpc) => void

  readonly waveEnemyCorpseCollector: oCNpc[]
  addWaveEnemyToCorpseCollector: (npc: oCNpc) => void
  disposeWaveEnemyCorpses: () => void

  readonly votesForPreparePhaseSkip: Player[]
  addVoteForPreparePhaseSkip: (player: Player) => void
  resetVotesForPreparePhaseSkip: () => void
  removeVoteForPreparePhaseSkip: (player: Player) => void

  readonly waveEnemiesWithoutAttackFocus: oCNpc[]
  addWaveEnemyWithoutAttackFocus: (npc: oCNpc) => void
  restartWaveEnemiesWithoutAttackFocus: () => void

  readonly currentWaveEnemiesKilled: number
  addKilledEnemy: () => void
  restartCurrentWaveEnemiesKilled: () => void

  readonly enemySpawnMultiplier: number
  setEnemySpawnMultiplier: (value: number) => void

  readonly captainGarond: oCNpc | null
  setCaptainGarond: (value: oCNpc) => void

  readonly currentWaveEnemies: oCNpc[]
  addCurrentWaveEnemy: (value: oCNpc) => void
  removeCurrentWaveEnemy: (value: number) => void
  clearCurrentWaveEnemies: () => void
}

SuperJSON.registerCustom<BaseUnionObject, JSONObject>(
  {
    isApplicable: (v): v is BaseUnionObject => v instanceof BaseUnionObject,
    serialize: (v) => ({ uuid: v.Uuid, type: v.constructor.name }),
    deserialize: (v) => {
      const className = v['type'] as string
      const classDefinition = (UnionClasses as any)[className]
      return new classDefinition(v['uuid'] as string)
    },
  },
  'BaseUnionObject',
)

const storage: PersistStorage<GameState> = {
  getItem: (name) => {
    if (process.env['APP_ENV'] == 'production') {
      return null
    }

    const fileContent = fs.readFileSync(`./tmp/${name}.storage`).toString()
    if (!fileContent) return null

    const state = superjson.parse(fileContent) as any
    if (!state) return null
    return state
  },
  setItem: (name, value) => {
    if (process.env['APP_ENV'] == 'production') {
      return
    }

    fs.writeFileSync(`./tmp/${name}.storage`, superjson.stringify(value))
  },
  removeItem: (name) => {
    if (process.env['APP_ENV'] == 'production') {
      return
    }

    fs.writeFileSync(`./tmp/${name}.storage`, superjson.stringify({}))
  },
}

export const store = createStore<GameState>()(
  persist(
    (set) => ({
      players: {},
      initPlayerAttributes: (playerId: number) =>
        set((state) => {
          const players = state.players
          players[playerId] = {
            role: 'player',
            learningPoints: 0,
            healTargetUuid: '',
            waveDamageDealt: 0,
            totalDamageDealt: 0,
            totalWaveEnemyKills: 0,
            totalWavesSurvived: 0,
            reviveTimeStart: -1,
            waveEnemyKills: 0,
            avatarUrl: '',
            hotbarSlots: [null, null, null, null, null, null, null, null, null],
            lastUsedWeapon: null,
          }
          return {
            players,
          }
        }),
      updatePlayerAttributes: (playerId: number, attrs: Partial<PlayerAttributes>) =>
        set((state) => {
          const players = state.players
          players[playerId] = Object.assign({}, players[playerId], attrs)
          return {
            players,
          }
        }),
      deletePlayerAttributes: (playerId: number) =>
        set((state) => {
          const players = state.players
          delete players[playerId]
          return {
            players,
          }
        }),
      gamePhase: 'INIT',
      setGamePhase: (value) => set((state) => ({ gamePhase: value })),

      waveCounter: 0,
      nextWave: () => set((state) => ({ waveCounter: state.waveCounter + 1 })),
      restartWaves: () => set((state) => ({ waveCounter: 0 })),

      lastWaveEndTime: null,
      setLastWaveEndTime: (value) => set((state) => ({ lastWaveEndTime: value })),
      setLastWaveEndTimeToZero: () => set((state) => ({ lastWaveEndTime: new Date(0) })),

      timeLeftToEndPreparePhase: 0,
      decrementTimeLeftToEndPreparePhase: () =>
        set((state) => ({ timeLeftToEndPreparePhase: state.timeLeftToEndPreparePhase - 1 })),
      setTimeLeftToEndPreparePhase: (value) =>
        set((state) => ({ timeLeftToEndPreparePhase: value })),
      restartTimeLeftToEndPreparePhase: () =>
        set((state) => ({ timeLeftToEndPreparePhase: PREPARE_PHASE_TIME })),

      waveEnemyStage: 0,
      nextWaveEnemyStage: () => set((state) => ({ waveEnemyStage: state.waveEnemyStage + 1 })),
      restartWaveEnemyStage: () => set((state) => ({ waveEnemyStage: 0 })),

      possiblePlayersToAttack: [],
      addPossiblePlayerToAttack: (npc: oCNpc) =>
        set((state) => ({
          possiblePlayersToAttack: [...state.possiblePlayersToAttack, npc],
        })),
      restartPossiblePlayerToAttack: () => set((state) => ({ possiblePlayersToAttack: [] })),
      removePossiblePlayerToAttack: (npc: oCNpc) =>
        set((state) => ({
          possiblePlayersToAttack: state.possiblePlayersToAttack.filter(
            (player) => player.Uuid !== npc.Uuid,
          ),
        })),

      waveEnemyCorpseCollector: [],
      addWaveEnemyToCorpseCollector: (value) =>
        set((state) => ({
          waveEnemyCorpseCollector: [...state.waveEnemyCorpseCollector, value],
        })),
      disposeWaveEnemyCorpses: () => set((state) => ({ waveEnemyCorpseCollector: [] })),

      votesForPreparePhaseSkip: [],
      addVoteForPreparePhaseSkip: (player) =>
        set((state) => ({
          votesForPreparePhaseSkip: [...state.votesForPreparePhaseSkip, player],
        })),
      resetVotesForPreparePhaseSkip: () => set((state) => ({ votesForPreparePhaseSkip: [] })),
      removeVoteForPreparePhaseSkip: (player) =>
        set((state) => ({
          votesForPreparePhaseSkip: state.votesForPreparePhaseSkip.filter((p) => p != player),
        })),

      currentWaveEnemyType: '',
      setCurrentWaveEnemyType: (enemy) => set((state) => ({ currentWaveEnemyType: enemy })),

      currentWaveEnemyTypeIndex: 0,
      incrementCurrentWaveEnemyTypeIndex: () =>
        set((state) => ({ currentWaveEnemyTypeIndex: state.currentWaveEnemyTypeIndex + 1 })),
      resetCurrentWaveEnemyTypeIndex: () => set(() => ({ currentWaveEnemyTypeIndex: 0 })),

      waveEnemiesWithoutAttackFocus: [],
      addWaveEnemyWithoutAttackFocus: (value) =>
        set((state) => ({
          waveEnemiesWithoutAttackFocus: [...state.waveEnemiesWithoutAttackFocus, value],
        })),
      restartWaveEnemiesWithoutAttackFocus: () =>
        set((state) => ({ waveEnemiesWithoutAttackFocus: [] })),

      waveEnemyIndex: 0,
      nextWaveEnemyIndex: () => set((state) => ({ waveEnemyIndex: state.waveEnemyIndex + 1 })),
      restartWaveEnemyIndex: () => set((state) => ({ waveEnemyIndex: 0 })),

      waveEnemyCount: BASE_WAVE_ENEMY_COUNT,
      setWaveEnemyCount: (value) => set((state) => ({ waveEnemyCount: value })),
      addWaveEnemyCount: () =>
        set((state) => ({
          waveEnemyCount:
            state.waveEnemyCount + Math.floor(BASE_WAVE_ENEMY_COUNT * state.enemySpawnMultiplier),
        })),

      currentBaseWaveEnemyCount: BASE_WAVE_ENEMY_COUNT,
      setCurrentBaseWaveEnemyCount: (value) =>
        set((state) => ({ currentBaseWaveEnemyCount: value })),

      currentWaveEnemiesKilled: 0,
      addKilledEnemy: () =>
        set((state) => ({ currentWaveEnemiesKilled: state.currentWaveEnemiesKilled + 1 })),
      restartCurrentWaveEnemiesKilled: () => set((state) => ({ currentWaveEnemiesKilled: 0 })),

      enemySpawnMultiplier: 1,
      setEnemySpawnMultiplier: (value) => set((state) => ({ enemySpawnMultiplier: value })),

      waveEnemySpawnLimit: BASE_ENEMY_SPAWN_LIMIT,
      setWaveEnemySpawnLimit: (value) => set(() => ({ waveEnemySpawnLimit: value })),

      captainGarond: null,
      setCaptainGarond: (value) => set((state) => ({ captainGarond: value })),

      currentWaveEnemies: [],
      addCurrentWaveEnemy: (value) =>
        set((state) => ({ currentWaveEnemies: [...state.currentWaveEnemies, value] })),
      removeCurrentWaveEnemy: (value) =>
        set((state) => ({
          currentWaveEnemies: state.currentWaveEnemies.splice(value, 1) && state.currentWaveEnemies,
        })),
      clearCurrentWaveEnemies: () => set((state) => ({ currentWaveEnemies: [] })),
    }),
    {
      name: 'game-store',
      storage,
    },
  ),
)

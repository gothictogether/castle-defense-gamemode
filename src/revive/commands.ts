import { NPC_ATR_HITPOINTS } from 'gothic-together/union/enums'
import { SetProgressBar } from 'src/utils/progress-bar.js'
import { MyPlayer as Player } from 'src/player.js'
import { GameMode } from 'src/gamemode.js'
import { ShowPopup } from 'src/utils/notifications.js'
import { Client } from 'gothic-together'

const Revive = (player: Player, args: any, gameMode: GameMode) => {
  const focusedNpc = player.Npc.GetFocusNpc()
  const revivingPlayerHP = player.Npc.GetAttribute(NPC_ATR_HITPOINTS)

  if (!focusedNpc || revivingPlayerHP == 0) {
    return
  }

  const startTime = new Date().getTime()
  player.SetAttrs({ reviveTimeStart: startTime })

  const focusedPlayer = gameMode.GetPlayerFromNpc(focusedNpc)
  if (focusedPlayer) {
    const targetCurrentHP = focusedPlayer.Npc.GetAttribute(NPC_ATR_HITPOINTS)

    if (targetCurrentHP == 0) {
      player.SetAttrs({ healTargetUuid: focusedNpc.Uuid })
      SetProgressBar(player, true, 'WSKRZESZENIE')
      player.Npc.GetModel()?.StartAni('T_PLUNDER', 1)
      Client.PlaySound3D(player.Id, 'SVM_8_GOODVICTORY.WAV', player.Npc)
    }
  }
}

const ReviveSuccess = (player: Player, args: any, gameMode: GameMode) => {
  const currentTime = new Date().getTime()

  if (player.Attrs.reviveTimeStart + 4500 > currentTime) {
    return
  }

  const focusedPlayer = player.Npc.GetFocusNpc()

  if (focusedPlayer?.Uuid != player.Attrs.healTargetUuid) {
    return
  }

  focusedPlayer.SetAttribute(NPC_ATR_HITPOINTS, 100)
  focusedPlayer.state()?.EndCurrentState()

  SetProgressBar(player, false)
  player.SetAttrs({ reviveTimeStart: -1 })

  const gamemodePlayer = gameMode.GetPlayerFromNpc(focusedPlayer)!
  ShowPopup(player, `Pomyślnie przywrócono życie ${gamemodePlayer.Name}`, 'success')
}

export default {
  Revive,
  ReviveSuccess,
}

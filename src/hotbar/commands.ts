import { Player } from 'gothic-together/player'
import { FindItemInInventory, UseItemFromInventory } from 'src/inventory/index.js'
import { DrawMagic, DrawMeleeWeapon, DrawRangeWeapon, PutAwayWeapon } from './index.js'
import { RemoveItemFromHotbar } from 'src/gamemode/hotbar.js'
import { Item } from 'src/inventory/item.js'

const UseSlot1 = (player: Player) => {
  HandleHotBarSlot(player, 0)
}
const UseSlot2 = (player: Player) => {
  HandleHotBarSlot(player, 1)
}
const UseSlot3 = (player: Player) => {
  HandleHotBarSlot(player, 2)
}
const UseSlot4 = (player: Player) => {
  HandleHotBarSlot(player, 3)
}
const UseSlot5 = (player: Player) => {
  HandleHotBarSlot(player, 4)
}
const UseSlot6 = (player: Player) => {
  HandleHotBarSlot(player, 5)
}
const UseSlot7 = (player: Player) => {
  HandleHotBarSlot(player, 6)
}
const UseSlot8 = (player: Player) => {
  HandleHotBarSlot(player, 7)
}
const UseSlot9 = (player: Player) => {
  HandleHotBarSlot(player, 8)
}
const RemoveItem = (player: Player, objectName: string) => {
  RemoveItemFromHotbar(player, objectName)
}

const UseLastWeapon = (player: Player) => {
  const IsWeaponDrawn = player.Npc.GetWeaponMode() != 0
  const weapon: Item | null = player.Attrs.lastUsedWeapon

  if (!weapon && IsWeaponDrawn) {
    PutAwayWeapon(player)
    return
  } else if (!weapon && !IsWeaponDrawn) {
    DrawMeleeWeapon(player)
    return
  }

  const isItemInInventory = FindItemInInventory(player, weapon!.objectName)

  if (!isItemInInventory) {
    player.SetAttrs({ lastUsedWeapon: null })
  } else if (isItemInInventory && weapon!.isEquipped == false) {
    UseItemFromInventory(player, weapon!.objectName)
  }

  if (IsWeaponDrawn) {
    PutAwayWeapon(player)
    return
  }

  if (weapon!.category == 'ITMW') {
    DrawMeleeWeapon(player)
  } else if (weapon!.category == 'ITRW') {
    DrawRangeWeapon(player, weapon!.objectName)
  } else if (weapon!.category == 'ITRU') {
    DrawMagic(player, weapon!.objectName)
  }
}

const HandleHotBarSlot = (player: Player, slotIndex: number) => {
  const slotItem: Item | null = player.Attrs.hotbarSlots[slotIndex]
  if (!slotItem) {
    return
  }

  const item = FindItemInInventory(player, player.Attrs.hotbarSlots[slotIndex].objectName!)
  if (!item) {
    return
  }

  if (!player.Npc.CanUse(item!.vob!)) {
    return
  }

  if (item.category == 'ITMW' || item.category == 'ITRW' || item.category == 'ITRU') {
    player.SetAttrs({ lastUsedWeapon: item })
  }

  if (!item.isEquipped) {
    UseItemFromInventory(player, slotItem.objectName!)

    if (!FindItemInInventory(player, item.objectName)) {
      RemoveItemFromHotbar(player, item.objectName)
    }
  }

  const isWeaponDrawn = player.Npc.GetWeaponMode() != 0
  if (item.category == 'ITMW') {
    if (isWeaponDrawn) {
      PutAwayWeapon(player)
      return
    }

    DrawMeleeWeapon(player)
  } else if (item.category == 'ITRW') {
    if (isWeaponDrawn) {
      PutAwayWeapon(player)
      return
    }

    DrawRangeWeapon(player, slotItem.objectName!)
  } else if (item.category == 'ITRU') {
    if (isWeaponDrawn) {
      PutAwayWeapon(player)
      return
    }

    DrawMagic(player, item.objectName)
  }
}

export default {
  UseSlot1,
  UseSlot2,
  UseSlot3,
  UseSlot4,
  UseSlot5,
  UseSlot6,
  UseSlot7,
  UseSlot8,
  UseSlot9,
  RemoveItem,
  UseLastWeapon,
}

import { Server } from 'gothic-together'
import { Player } from 'gothic-together/player'
import { oCMsgWeapon } from 'gothic-together/union/classes/oCMsgWeapon'
import {
  NPC_WEAPON_BOW,
  NPC_WEAPON_CBOW,
  NPC_WEAPON_FIST,
  NPC_WEAPON_MAG,
  TWeaponSubType,
} from 'gothic-together/union/enums'
import { Item } from 'src/inventory/item.js'

export const PutAwayWeapon = (player: Player) => {
  const eventMan = player.Npc.eventManager()
  const drawWeaponMessage = oCMsgWeapon.oCMsgWeapon_OnInit2(TWeaponSubType.EV_REMOVEWEAPON, 0, 0)
  eventMan!.OnMessage(drawWeaponMessage!, player.Npc)
}

export const DrawMeleeWeapon = (player: Player) => {
  const eventMan = player.Npc.eventManager()
  const drawWeaponMessage = oCMsgWeapon.oCMsgWeapon_OnInit2(
    TWeaponSubType.EV_DRAWWEAPON,
    NPC_WEAPON_FIST,
    0,
  )
  eventMan!.OnMessage(drawWeaponMessage!, player.Npc)
}

export const DrawRangeWeapon = (player: Player, weaponName: string) => {
  const eventMan = player.Npc.eventManager()
  const drawWeaponMessage = oCMsgWeapon.oCMsgWeapon_OnInit2(
    TWeaponSubType.EV_DRAWWEAPON,
    weaponName!.includes('CROSSBOW') ? NPC_WEAPON_CBOW : NPC_WEAPON_BOW,
    0,
  )
  eventMan!.OnMessage(drawWeaponMessage!, player.Npc)
}

export const DrawMagic = (player: Player, weaponName: string) => {
  Server.SetActiveSpell(weaponName, player.Npc)
  const eventMan = player.Npc.eventManager()
  const drawWeaponMessage = oCMsgWeapon.oCMsgWeapon_OnInit2(
    TWeaponSubType.EV_DRAWWEAPON,
    NPC_WEAPON_MAG,
    0,
  )
  eventMan!.OnMessage(drawWeaponMessage!, player.Npc)
}

export const AddHotbarItemOnUse = (player: Player, item: Item) => {
  const currentHotbarslots = player.Attrs.hotbarSlots

  if (IsItemOnHotbar(player, item)) {
    return
  }
  
  if (item.category == 'ITMW') {
    currentHotbarslots[0] = item
  } else if (item.category == 'ITRW') {
    currentHotbarslots[1] = item
  } else if (item.category == 'ITRU') {
    for (let i = 2; i < 9; i++) {
      if (!currentHotbarslots[i] && !currentHotbarslots.includes(item)) {
        currentHotbarslots[i] = item
        return
      }
    }
  }

  player.SetAttrs({ hotbarSlots: currentHotbarslots })
}

export const RemoveHotbarItemOnUse = (player: Player, item: Item) => {
  const currentHotbarslots = player.Attrs.hotbarSlots

  for (let i = 0; i < 9; i++) {
    if (currentHotbarslots[i] == item) {
      currentHotbarslots[i] = null
    }
  }

  player.SetAttrs({ hotbarSlots: currentHotbarslots })
}

const IsItemOnHotbar = (player: Player, item: Item) => {
  for (let i = 0; i < 9; i++) {
    if (player.Attrs.hotbarSlots[i] == item) {
      return true
    }
  }
  return false
}

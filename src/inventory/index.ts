import { Server } from 'gothic-together'
import { Player } from 'gothic-together/player'
import {
  NPC_ATR_DEXTERITY,
  NPC_ATR_HITPOINTSMAX,
  NPC_ATR_MANAMAX,
  NPC_ATR_STRENGTH,
  NPC_WEAPON_NONE,
  oCMsgManipulate,
  oTEnumNpcTalent,
  TManipulateSubType,
} from 'gothic-together/union/classes/index'
import { Item } from './item.js'
import { isItemActive } from './utils.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'
import { InventoryState } from 'react/src/pages/Equipment/index.js'
import { HeroPageState } from 'react/src/pages/Hero/index.js'
import { RemoveItemFromHotbar } from 'src/gamemode/hotbar.js'
import { ShowPopup } from 'src/utils/notifications.js'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { AddHotbarItemOnUse, RemoveHotbarItemOnUse } from 'src/hotbar/index.js'

const PlayerInventory: Record<number, Item[]> = {}

export const InitPlayerInventory = (player: Player): void => {
  PlayerInventory[player.Id] = []
}

export const AddItemToInventory = (player: Player, item: Item): Item | null => {
  if (!item) {
    return null
  }

  if (!PlayerInventory[player.Id]) {
    InitPlayerInventory(player)
  }

  const searchedItem = FindItemInInventory(player, item.objectName)

  if (!searchedItem) {
    item.owner = player.Npc

    if (!item.vob && item.isNative) {
      item.vob = Server.PutInInventory(item.objectName, item.owner, item.amount)
    }

    PlayerInventory[player.Id]!.push(item)
  } else {
    searchedItem.amount += item.amount

    PlayerInventory[player.Id] = PlayerInventory[player.Id]!.map((item) =>
      item.objectName == searchedItem.objectName ? searchedItem : item,
    )
  }
  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })

  return item
}

export const FindItemInInventory = (player: Player, itemName: string): Item | null => {
  const item = PlayerInventory[player.Id]?.find((i) => i.objectName == itemName)

  return item || null
}

export const RemoveItemFromInventory = (
  player: Player,
  itemName: string,
  amountToRemove?: number,
): void => {
  const item: Item | null = FindItemInInventory(player, itemName)

  if (!item) {
    return
  }

  if (amountToRemove && amountToRemove > 0) {
    item.amount -= amountToRemove

    if (item.amount <= 0) {
      PlayerInventory[player.Id] = PlayerInventory[player.Id]!.filter(
        (i) => i.objectName != itemName,
      )
    } else {
      PlayerInventory[player.Id] = PlayerInventory[player.Id]!.map((i) =>
        i.objectName == itemName ? item : i,
      )
    }
  } else {
    PlayerInventory[player.Id] = PlayerInventory[player.Id]!.filter((i) => i.objectName != itemName)
  }
}

export const GetPlayerInventory = (player: Player) => {
  if (!PlayerInventory[player.Id]) {
    return []
  }

  return PlayerInventory[player.Id]!
}

export const GetPlayerGold = (player: Player) => {
  const gold = FindItemInInventory(player, 'ITMI_GOLD')
  return gold ? gold.amount : 0
}

export const UseItemFromInventory = (player: Player, objectName: string) => {
  const item: Item | null = FindItemInInventory(player, objectName!)

  if (!item || player.Npc.GetWeaponMode() != NPC_WEAPON_NONE) {
    ShowPopup(player, 'Schowaj broń przed użyciem przedmiotu', 'failed')
    return
  }

  if (
    item.category == 'ITRI' ||
    item.category == 'ITBE' ||
    item.category == 'ITAM' ||
    item.category == 'ITAR' ||
    item.category == 'ITMW' ||
    item.category == 'ITRW' ||
    item.category == 'ITSC' ||
    item.category == 'ITRU'
  ) {
    if (player.Npc.CanUse(item.vob!)) {
      player.Npc.Equip(item.vob!)
      item.isEquipped = isItemActive(item)
      PlayerInventory[player.Id]?.forEach((i) => {
        if (i.isEquipped && i.category == item.category && item != i) {
          if (item.category != 'ITRU' && item.category != 'ITSC') {
            i.isEquipped = isItemActive(i)
          }
        }
      })

      if (item.isEquipped) {
        AddHotbarItemOnUse(player, item)
      } else if (!item.isEquipped) {
        RemoveHotbarItemOnUse(player, item)
      }
    }
  } else if (item.category == 'ITPL' || item.category == 'ITFO' || item.category == 'ITPO') {
    const aniCtrl = player.Npc.GetAnictrl()!
    if (!aniCtrl.IsStanding()) {
      return
    }

    const message = oCMsgManipulate.oCMsgManipulate_OnInit6(
      TManipulateSubType.EV_USEITEM,
      item.vob!,
      1,
    )

    player.Npc.GetModel()?.StopAnisLayerRange(0, 99999)
    const eventMan = player.Npc!.eventManager()!
    eventMan.Clear()
    eventMan.OnMessage(message!, player.Npc!)

    if (item.amount! > 1) {
      item.amount -= 1
    } else {
      RemoveItemFromInventory(player, objectName)
    }
  }

  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })

  UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
    hotbarSlots: player.Attrs.hotbarSlots,
  })

  if (item.category == 'ITAM' || item.category == 'ITRI' || item.category == 'ITMW')
    UpdateOverlayState<HeroPageState>(player, 'HeroComponent', {
      hitpoints: player.Npc.GetAttribute(NPC_ATR_HITPOINTSMAX)!,
      mana: player.Npc.GetAttribute(NPC_ATR_MANAMAX)!,
      strength: player.Npc.GetAttribute(NPC_ATR_STRENGTH)!,
      dexterity: player.Npc.GetAttribute(NPC_ATR_DEXTERITY)!,
      '1h': player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_1H)!,
      '2h': player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_2H)!,
      bow: player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_BOW)!,
      cbow: player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_CROSSBOW)!,
    })
}

export const DropItemFromInventory = (player: Player, objectName: string) => {
  const item: Item | null = FindItemInInventory(player, objectName!)
  if (!item || player.Npc.GetWeaponMode() != NPC_WEAPON_NONE) {
    ShowPopup(player, 'Schowaj broń przed wyrzuceniem', 'failed')
    return
  }

  RemoveItemFromInventory(player, objectName!, 1)

  if (item.isEquipped) {
    player.Npc.UnequipItem(item.vob!)
    item.isEquipped = false
  }

  if (item == player.Attrs.lastUsedWeapon) {
    player.SetAttrs({ lastUsedWeapon: null })
  }

  item.vob?.RemoveVobFromWorld()
  RemoveItemFromHotbar(player, item.objectName)

  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })
}

export const UnequipAllItems = (player: Player) => {
  for (const item of PlayerInventory[player.Id]!) {
    if (item.isEquipped) {
      player.Npc.UnequipItem(item.vob!)
      item.isEquipped = false
    }
  }

  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })
}

export const UnequipWeapons = (player: Player) => {
  for (const item of PlayerInventory[player.Id]!) {
    if (
      item.isEquipped &&
      (item.category == 'ITMW' ||
        item.category == 'ITRW' ||
        item.category == 'ITRU' ||
        item.category == 'ITSC')
    ) {
      player.Npc.UnequipItem(item.vob!)
      item.isEquipped = false
    }
  }

  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })
}

import { MyPlayer as Player } from 'src/player.js'
import { GameMode, REACT_BASE_URL } from 'src/gamemode.js'
import {
  DropItemFromInventory,
  FindItemInInventory,
  GetPlayerInventory,
  UseItemFromInventory,
} from './index.js'
import { Client } from 'gothic-together'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'

type THotBarItem = {
  objectName: string
  index: number
}

const Eq = (player: Player, args: any, gameMode: GameMode) => {
  Client.NavigateHtmlComponent(player.Id, 'Main', `${REACT_BASE_URL}#eq`)
  Client.SendEventToHtmlComponents(player.Id, 'EqUpdate', GetPlayerInventory(player))
}

const AddItemToHotBar = (player: Player, hotbarItem: THotBarItem, gameMode: GameMode) => {
  const objectName: string = hotbarItem.objectName
  const index: number = hotbarItem.index

  const item = FindItemInInventory(player, objectName!)

  if (!item) {
    return
  }

  const currentHotbarItems = player.Attrs.hotbarSlots

  if (currentHotbarItems.includes(item)) {
    const index = currentHotbarItems.findIndex((element) => element == item)

    currentHotbarItems[index] = null
    player.SetAttrs({ hotbarSlots: currentHotbarItems })
  }

  const hotBarSlots = player.Attrs.hotbarSlots

  hotBarSlots[index] = item

  UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
    hotbarSlots: player.Attrs.hotbarSlots,
  })
}

const UseItem = (player: Player, args: any, gameMode: GameMode) => {
  UseItemFromInventory(player, args)
}
const DropItem = (player: Player, args: any, gameMode: GameMode) => {
  DropItemFromInventory(player, args)
}

export default {
  Eq,
  UseItem,
  DropItem,
  AddItemToHotBar,
}

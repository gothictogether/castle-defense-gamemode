import { MiddlewareBase } from 'gothic-together/middleware-base'
import { oCItem } from 'gothic-together/union/classes/index'
import { MyPlayer as Player } from 'src/player.js'
import { AddItemToInventory } from './index.js'
import { CreateItem } from './item.js'

export class InventoryMiddleware extends MiddlewareBase {
  override async OnPlayerTakeItem(player: Player, item: oCItem, itemName: string) {
    if (!item || !itemName) {
      return
    }

    AddItemToInventory(player, CreateItem(itemName, item.amount() ?? 1))
    item.RemoveVobFromWorld()
  }
}

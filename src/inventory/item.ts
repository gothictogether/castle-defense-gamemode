import { oCItem, oCNpc, zVEC3 } from 'gothic-together/union/classes/index'

export type Item = {
  objectName: string
  category?: string
  amount: number
  isNative: boolean
  positionWorld?: zVEC3
  owner?: oCNpc
  vob?: oCItem | null
  isEquipped: boolean
  price?: number
}

export const CreateItem = (objectName: string, amount: number) => {
  const newItem: Item = {
    isNative: true,
    objectName,
    category: GetItemCategory(objectName),
    amount,
    isEquipped: false,
  }

  return newItem
}

const GetItemCategory = (objectName: string) => {
  return objectName.substring(0, 4)
}

import { ShowNotification } from './utils/notifications.js'
import { Server, Client, GameModeBase, zVEC3 } from 'gothic-together'
import { BindCommandType } from 'gothic-together/client-commands'
import { oCNpc, zCOLOR } from 'gothic-together/union/classes/index'
import { CallCommand } from './utils/commands-handler.js'
import { InitPlayerInventory } from './inventory/index.js'
import { MyPlayer as Player } from 'src/player.js'
import { GameState, store } from './store.js'
import { RestartGamemode } from './utils/restart-gamemode.js'
import { ShowScoreboard } from './utils/scoreboard.js'
import { UpdatePartyMemberOverlay, UpdateQuestOverlay } from './overlay/updater.js'
import { HandleCaptainGarondState, SpawnGarond } from './gamemode/garond.js'
import { CheckWaveStatus, HandlePreparePhase } from './gamemode/wave-handler.js'
import { ManageEnemiesWithoutFocus } from './gamemode/enemy-handler.js'
import { HandleEnemyState, SetWaveEnemyType, UpdateEnemyEvent } from './gamemode/enemy-handler.js'
import { HandleEnemySpawning } from './gamemode/enemy-handler.js'
import { TeleportGarondFarAwayToHome } from './gamemode/teleport.js'
import { TeleportEnemyFarAwayToHome } from './gamemode/teleport.js'
import { SetDifficulty } from './gamemode/difficulty.js'
import {
  CheckPlayersAliveStatus,
  HandlePlayerDeath,
  UpdatePlayerClientConfig,
  UpdatePossiblePlayersToAttack,
} from './gamemode/player.js'
import { EquipStartItems } from './gamemode/inventory.js'
import { attributes, InitializePlayerAttributes } from './gamemode/attributes.js'
import { GetCurrentWaveEnemyIndex, IsActivePhase } from './gamemode/utils.js'
import { UpdateOverlayPlayerCount } from './gamemode/updater.js'
import { UpdateOverlayPreparePhase } from './gamemode/updater.js'
import { ENEMY_ATTACK_DISTANCE_THRESHOLD } from './gamemode/wave-constants.js'
import { GrantBaseRewardOnJoin } from './gamemode/reward.js'
import { RemoveDefaultVobs } from './gamemode/world.js'

export const REACT_BASE_URL = process.env['FRONTEND_URL']
const GAMEMODE_AUTORESTART = process.env['AUTO_RESTART'] == 'true'

const PLAYERS_SPAWN_POS = new zVEC3([1258.548, 247.738, -293.938])
const TOT_POS = new zVEC3([12194.784, -1468.219, 22960.459])

export class GameMode extends GameModeBase {
  override async OnInitServer() {
    store.setState(store.getInitialState())

    Server.SetPlayersSpawnPos(PLAYERS_SPAWN_POS)
    Server.SetHostSpawnPos(GAMEMODE_AUTORESTART ? TOT_POS : PLAYERS_SPAWN_POS)

    SpawnGarond(this)
    Server.CreateObject('ORC_SQUAREPLANKS_2X3M.3DS', new zVEC3([-2045, 248, -830]))
  }

  override async OnPlayerJoinServer(player: Player) {
    this.state.initPlayerAttributes(player.Id)
    player.SetAttrs({
      learningPoints: 0,
      totalDamageDealt: 0,
      waveDamageDealt: 0,
      totalWaveEnemyKills: 0,
      totalWavesSurvived: 0,
      healTargetUuid: '',
      role: player.Id == 0 ? 'admin' : 'player',
      avatarUrl: Server.GetPlayerAvatar(player.Id) ?? '',
    })

    UpdatePlayerClientConfig(player)
    UpdatePartyMemberOverlay(this)
    UpdateQuestOverlay(this, player)

    Client.BindCommandToKeyPress(player.Id, 0x1b, 'menu', BindCommandType.OVERLAY_TOGGLE)
    Client.BindCommandToKeyPress(player.Id, 0x09, 'eq', BindCommandType.OVERLAY_TOGGLE)
    Client.BindCommandToKeyPress(player.Id, 0x4e, 'hero', BindCommandType.OVERLAY_TOGGLE)
    Client.BindCommandToKeyPress(player.Id, 0x70, 'shop', BindCommandType.OVERLAY_TOGGLE)
    Client.BindCommandToKeyPress(player.Id, 0x71, 'revive')
    Client.BindCommandToKeyPress(player.Id, 0x72, 'creator', BindCommandType.OVERLAY_TOGGLE)

    Client.BindCommandToKeyPress(player.Id, 0x31, 'useslot1')
    Client.BindCommandToKeyPress(player.Id, 0x32, 'useslot2')
    Client.BindCommandToKeyPress(player.Id, 0x33, 'useslot3')
    Client.BindCommandToKeyPress(player.Id, 0x34, 'useslot4')
    Client.BindCommandToKeyPress(player.Id, 0x35, 'useslot5')
    Client.BindCommandToKeyPress(player.Id, 0x36, 'useslot6')
    Client.BindCommandToKeyPress(player.Id, 0x37, 'useslot7')
    Client.BindCommandToKeyPress(player.Id, 0x38, 'useslot8')
    Client.BindCommandToKeyPress(player.Id, 0x39, 'useslot9')
    Client.BindCommandToKeyPress(player.Id, 0x39, 'useslot9')
    Client.BindCommandToKeyPress(player.Id, 0x20, 'uselastweapon')

    Client.LoadHtmlComponent(player.Id, 'Main', `${REACT_BASE_URL}#eq`)
    Client.CreateHtmlComponent(player.Id, 'Main')

    Client.LoadHtmlComponent(player.Id, 'Overlay_Main', `${REACT_BASE_URL}#overlay`)
    Client.CreateHtmlComponent(player.Id, 'Overlay_Main')

    InitPlayerInventory(player)
    InitializePlayerAttributes(player, attributes)
    EquipStartItems(player)
    GrantBaseRewardOnJoin(player, this.state)
    RemoveDefaultVobs(player)

    player.Npc.SetGuild(1)

    SetDifficulty(this)
    UpdateOverlayPlayerCount(this)

    Server.SendMessageToAll(`Witamy nowego gracza ${player.Name}!`)
    Server.SendMessageToPlayer(player.Id, `Cześć ${player.Name}!`, new zCOLOR([255, 0, 0, 255]))
  }

  override async OnPlayerDisconnectServer(player: Player) {
    this.state.deletePlayerAttributes(player.Id)

    if (this.Players.length == 0 && GAMEMODE_AUTORESTART) {
      RestartGamemode(this)
    }

    if (this.state.votesForPreparePhaseSkip.includes(player)) {
      this.state.removeVoteForPreparePhaseSkip(player)
    }

    UpdatePartyMemberOverlay(this)
    UpdateOverlayPlayerCount(this)
    SetDifficulty(this)
  }

  override async OnPlayerCommand(player: Player, commandName: string, args: any) {
    if (CallCommand(commandName, player, args, this)) {
      return
    }

    switch (commandName) {
      case 'startgame': {
        if (
          (!this.state.votesForPreparePhaseSkip.includes(player) &&
            this.state.gamePhase == 'PREPARE') ||
          this.state.gamePhase == 'INIT'
        ) {
          this.state.addVoteForPreparePhaseSkip(player)

          const ThereArePlayersWhoDidNotVote =
            this.state.votesForPreparePhaseSkip.length != this.Players.length

          if (this.state.waveCounter != 0 && ThereArePlayersWhoDidNotVote) {
            return
          }

          if (this.state.gamePhase == 'INIT') {
            SetWaveEnemyType(this.state)
            this.state.setGamePhase('ACTIVE')
            this.state.nextWave()

            ShowNotification(
              this.Players,
              `Nadciąga ${this.state.waveCounter} Fala`,
              'Przygotuj się do kolejnego natarcia przeciwnika!',
              3,
            )

            this.state.clearCurrentWaveEnemies()

            for (const p of this.Players) {
              UpdateQuestOverlay(this, p)
              p.SetAttrs({ totalWavesSurvived: p.Attrs.totalWavesSurvived + 1 })
              Client.PlaySound3D(p.Id, 'DIA_PYROKAR_TEST_15_00.WAV', p.Npc)
            }
          } else if (this.state.gamePhase == 'PREPARE') {
            this.state.setLastWaveEndTimeToZero()
            this.state.setTimeLeftToEndPreparePhase(0)
          }

          UpdateOverlayPreparePhase(this)
        }

        break
      }

      case 'menu': {
        Client.NavigateHtmlComponent(player.Id, 'Main', `${REACT_BASE_URL}#`)
        break
      }

      case 'killme': {
        player.Npc.SetAttribute(0, 0)
        break
      }

      case 'myping': {
        Server.SendMessageToPlayer(player.Id, `Twoj ping to: ${Server.GetPlayerPing(player.Id)}`)
        break
      }

      case 'opendoor': {
        const door = Server.FindVob('ORC_SQUAREPLANKS_2X3M.3DS', new zVEC3([-2045, 248, -830]))
        if (door) {
          door.RemoveVobFromWorld()
        }
        break
      }

      case 'closedoor': {
        const door = Server.FindVob('ORC_SQUAREPLANKS_2X3M.3DS', new zVEC3([-2045, 248, -830]))
        if (!door) {
          Server.CreateObject('ORC_SQUAREPLANKS_2X3M.3DS', new zVEC3([-2045, 248, -830]))
        }
        break
      }

      case 'reload': {
        Client.RemoveHtmlComponent(player.Id, 'Main')
        Client.RemoveHtmlComponent(player.Id, 'Overlay_Main')

        Client.LoadHtmlComponent(player.Id, 'Main', `${REACT_BASE_URL}#eq`)
        Client.CreateHtmlComponent(player.Id, 'Main')

        Client.LoadHtmlComponent(player.Id, 'Overlay_Main', `${REACT_BASE_URL}#overlay`)
        Client.CreateHtmlComponent(player.Id, 'Overlay_Main')
        break
      }

      case 'respawn': {
        player.Npc.SetCollDet(0)
        player.Npc.SetPositionWorld(new zVEC3([0, 500, 0]))
        player.Npc.SetCollDet(1)
        break
      }

      case 'exitgame': {
        Client.ExitGame(player.Id)
        break
      }

      default:
        return
    }
  }

  override async OnTick() {
    if (IsActivePhase(this.state)) {
      UpdatePossiblePlayersToAttack(
        this,
        this.state.captainGarond!,
        ENEMY_ATTACK_DISTANCE_THRESHOLD,
      )
      ManageEnemiesWithoutFocus(this.state)
      HandleEnemySpawning(this.state)
      UpdateEnemyEvent(this)
      TeleportEnemyFarAwayToHome(this)
    }

    CheckWaveStatus(this)
    HandlePreparePhase(this)
    CheckPlayersAliveStatus(this)
    TeleportGarondFarAwayToHome(this)
  }

  override async OnPlayerChangeAttribute(
    player: Player,
    attribute: number,
    currentValue: number,
    change: number,
  ) {
    UpdatePartyMemberOverlay(this)
    HandlePlayerDeath(this, attribute, currentValue, player)
  }

  override async OnDamage(
    attacker: oCNpc,
    attrackerName: string,
    target: oCNpc,
    targetName: string,
    weaponName: string,
    damage: number,
  ) {
    HandleCaptainGarondState(this, target)

    const attackerPlayer = this.GetPlayerFromNpc(attacker)
    const index = GetCurrentWaveEnemyIndex(this.state, target)

    HandleEnemyState(this, index, target, attackerPlayer!, damage)

    const targetPlayer = this.GetPlayerFromNpc(target)
    if (targetPlayer) {
      UpdatePartyMemberOverlay(this)
    }
  }

  ClearCurrentWaveEnamies() {
    for (const npc of this.state.currentWaveEnemies) {
      npc.RemoveVobFromWorld()
    }

    this.state.clearCurrentWaveEnemies()
  }

  get state() {
    return store.getState()
  }

  update(
    change: GameState | Partial<GameState> | ((state: GameState) => GameState | Partial<GameState>),
  ) {
    store.setState(change)
  }
}

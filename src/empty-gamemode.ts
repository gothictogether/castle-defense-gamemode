import { GameModeBase } from 'gothic-together'
import { Player } from 'gothic-together/player'
import { oCItem, oCMobInter, oCNpc, zCVob } from 'gothic-together/union/classes/index'

export class GameMode extends GameModeBase {
  override async OnInitServer() {
    console.log(`OnInitServer`)
  }

  override async OnExitServer() {
    console.log(`OnExitServer`)
  }

  override async OnPlayerJoinServer(player: Player) {
    console.log(`OnPlayerJoinServer ${player}`)
  }

  override async OnTick() {
    console.log(`OnTick`)
  }

  override async OnTime(day: number, hour: number, minute: number) {
    console.log(`OnTime ${day} ${hour} ${minute}`)
  }

  override async OnDamage(
    attacker: oCNpc,
    attrackerName: string,
    targe: oCNpc,
    targetName: string,
    weaponName: string,
    damage: number,
  ) {
    console.log(
      `OnDamage ${attacker} ${attrackerName} ${targe} ${targetName} ${weaponName} ${damage}`,
    )
  }

  override async OnPlayerDisconnectServer(player: Player) {
    console.log(`OnPlayerDisconnectServer ${player}`)
  }

  override async OnPlayerChatMessage(player: Player, message: string) {
    console.log(`OnPlayerChatMessage ${player} ${message}`)
  }

  override async OnPlayerMobInteract(player: Player, mob: oCMobInter, mobName: string) {
    console.log(`OnPlayerMobInteract ${player} ${mob} ${mobName}`)
  }

  override async OnPlayerNpcInteract(player: Player, npc: oCNpc, npcName: string) {
    console.log(`OnPlayerNpcInteract ${player} ${npc} ${npcName}`)
  }

  override async OnPlayerChangeFocus(player: Player, vob: zCVob, vobName: string, vobType: number) {
    console.log(`OnPlayerChangeFocus ${player} ${vob} ${vobName} ${vobType}`)
  }

  override async OnPlayerChangeAttribute(
    player: Player,
    attribute: number,
    currentValue: number,
    change: number,
  ) {
    console.log(`OnPlayerChangeAttribute ${player} ${attribute} ${currentValue} ${change}`)
  }

  override async OnPlayerChangeWeaponMode(player: Player, weaponMode: number) {
    console.log(`OnPlayerChangeWeaponMode ${player} ${weaponMode}`)
  }

  override async OnPlayerTakeItem(player: Player, item: oCItem, itemName: string) {
    console.log(`OnPlayerTakeItem ${player} ${item} ${itemName}`)
  }

  override async OnPlayerDropItem(player: Player, item: oCItem, itemName: string) {
    console.log(`OnPlayerDropItem ${player} ${item} ${itemName}`)
  }

  override async OnPlayerEquipItem(player: Player, item: oCItem, itemName: string) {
    console.log(`OnPlayerEquipItem ${player} ${item} ${itemName}`)
  }

  override async OnPlayerUnequipItem(player: Player, item: oCItem, itemName: string) {
    console.log(`OnPlayerUnequipItem ${player} ${item} ${itemName}`)
  }

  override async OnPlayerUseItem(player: Player, item: oCItem, itemName: string) {
    console.log(`OnPlayerUseItem ${player} ${item} ${itemName}`)
  }
}

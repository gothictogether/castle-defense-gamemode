import { HeroPageState } from 'react/src/pages/Hero/index.js'
import { MyPlayer as Player } from '../player.js'
import { GameMode } from 'src/gamemode.js'
import {
  NPC_ATR_HITPOINTSMAX,
  NPC_ATR_MANAMAX,
  NPC_ATR_STRENGTH,
  NPC_ATR_DEXTERITY,
  oTEnumNpcTalent,
} from 'gothic-together/union/enums'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { CreatorState } from 'react/src/pages/Creator/index.js'
import { ShopState } from 'react/src/pages/Shop/index.js'
import { Client } from 'gothic-together'
import { UpdatePartyMemberOverlay, UpdateQuestOverlay } from './updater.js'
import { FindItemInInventory, GetPlayerInventory } from 'src/inventory/index.js'
import { InventoryState } from 'react/src/pages/Equipment/index.js'
import { Item } from 'src/inventory/item.js'
import { AppState } from 'react/src/pages/Home/index.js'

const RequestStateHeroComponent = (player: Player, args: any, gamemode: GameMode) => {
  UpdateOverlayState<HeroPageState>(player, 'HeroComponent', {
    lp: player.Attrs.learningPoints,
    hitpoints: player.Npc.GetAttribute(NPC_ATR_HITPOINTSMAX)!,
    mana: player.Npc.GetAttribute(NPC_ATR_MANAMAX)!,
    strength: player.Npc.GetAttribute(NPC_ATR_STRENGTH)!,
    dexterity: player.Npc.GetAttribute(NPC_ATR_DEXTERITY)!,
    '1h': player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_1H)!,
    '2h': player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_2H)!,
    bow: player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_BOW)!,
    cbow: player.Npc.GetHitChance(oTEnumNpcTalent.NPC_TAL_CROSSBOW)!,
    mageCircle: player.Npc.GetTalentSkill(oTEnumNpcTalent.NPC_TAL_MAGE)!,
  })
}

const RequestStateOverlayComponent = (player: Player, args: any, gamemode: GameMode) => {
  UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
    playerId: player.Id,
    partyMembers: [],
    quests: [],
    remainingPrepareTime: gamemode.state.timeLeftToEndPreparePhase,
    votesYes: gamemode.state.votesForPreparePhaseSkip.length,
    playerCount: gamemode.Players.length,
    currentWave: gamemode.state.waveCounter,
    hotbarSlots: player.Attrs.hotbarSlots,
  })

  UpdatePartyMemberOverlay(gamemode)
  UpdateQuestOverlay(gamemode, player)
}

const RequestStateHomePage = (player: Player, args: any, gamemode: GameMode) => {
  UpdateOverlayState<AppState>(player, 'HomePage', {
    currentWave: gamemode.state.waveCounter,
  })

  UpdatePartyMemberOverlay(gamemode)
  UpdateQuestOverlay(gamemode, player)
}

const RequestStateCreatorComponent = (player: Player, args: any, gamemode: GameMode) => {
  UpdateOverlayState<CreatorState>(player, 'CreatorComponent', {
    username: player.Name,
    gender: 1,
    currBodyVar: 0,
    face: 0,
    faceShape: 0,
    fatness: 0,
  })
}

const RequestStateShopComponent = (player: Player, args: any, gamemode: GameMode) => {
  const gold = FindItemInInventory(player, 'ITMI_GOLD')
  const amount = gold ? gold.amount : 0
  UpdateOverlayState<ShopState>(player, 'ShopComponent', {
    playerGold: amount,
  })
}

const RequestStateInventoryComponent = (player: Player, args: any, gameMode: GameMode) => {
  UpdateOverlayState<InventoryState>(player, 'InventoryComponent', {
    inventory: GetPlayerInventory(player),
  })
}

const StopIntercept = (player: Player, args: any, gamemode: GameMode) => {
  Client.StopIntercept(player.Id)
}

export default {
  RequestStateHeroComponent,
  RequestStateOverlayComponent,
  RequestStateCreatorComponent,
  RequestStateShopComponent,
  RequestStateInventoryComponent,
  StopIntercept,
  RequestStateHomePage,
}

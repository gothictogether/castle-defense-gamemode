import { AppState } from 'react/src/pages/Home/index.js'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { GameMode } from 'src/gamemode.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'

export const UpdateOverlayPreparePhase = (gameMode: GameMode) => {
  for (const p of gameMode.Players) {
    UpdateOverlayState<OverlayState>(p, 'OverlayComponent', {
      currentWave: gameMode.state.waveCounter,
      remainingPrepareTime: gameMode.state.timeLeftToEndPreparePhase,
      votesYes: gameMode.state.votesForPreparePhaseSkip.length,
      playerCount: gameMode.Players.length,
    })
    UpdateOverlayState<AppState>(p, 'HomePage', {
      currentWave: gameMode.state.waveCounter,
    })
  }
}

export const UpdateOverlayPlayerCount = (gameMode: GameMode) => {
  for (const p of gameMode.Players) {
    UpdateOverlayState<OverlayState>(p, 'OverlayComponent', {
      playerCount: gameMode.Players.length,
    })
  }
}

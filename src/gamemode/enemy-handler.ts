import { GameState } from 'src/store.js'
import { InitializeWaveEnemy, MoveToGarond } from './wave-handler.js'
import { WAVE_ENEMY_STAGES } from './difficulty.js'
import { GameMode } from 'src/gamemode.js'
import ENEMY_SPAWN_MAP, { GOTOVOB_MSG_ID } from './wave-constants.js'
import { oCNpc } from 'gothic-together/union/classes/oCNpc'
import { AreAllEnemiesFocusedOnAttack, GetRandomSpawnVector, SelectNearbyPlayer } from './utils.js'
import { Server } from 'gothic-together'
import { Player } from 'gothic-together/player'
import { NPC_ATR_HITPOINTS } from 'gothic-together/union/enums'
import { UpdateQuestOverlay } from 'src/overlay/updater.js'

export const HandleEnemySpawning = (state: GameState) => {
  if (
    state.waveEnemyIndex < state.waveEnemyCount &&
    state.currentWaveEnemies.length < state.waveEnemySpawnLimit
  ) {
    state.nextWaveEnemyIndex()

    SpawnWaveEnemy(state)
  }
}

export const SetWaveEnemyType = (state: GameState) => {
  const enemy = WAVE_ENEMY_STAGES[state.waveEnemyStage]![state.currentWaveEnemyTypeIndex]!

  state.setCurrentWaveEnemyType(enemy)
}

export const UpdateEnemyEvent = (gameMode: GameMode) => {
  for (const e of gameMode.state.currentWaveEnemies) {
    const eventMan = e!.eventManager()
    const msg = eventMan!.GetActiveMessage()

    if (
      msg?.GetMessageID() != GOTOVOB_MSG_ID &&
      !e.enemy() &&
      gameMode.state.possiblePlayersToAttack.length == 0
    ) {
      MoveToGarond(gameMode.state, e)
    } else if (
      gameMode.state.possiblePlayersToAttack.length > 0 &&
      e.enemy()?.Uuid == gameMode.state.captainGarond!.Uuid
    ) {
      Server.NpcAttack(e, SelectNearbyPlayer(gameMode)!)
    }
  }
}

export const HandleEnemyState = (
  gameMode: GameMode,
  enemyIndex: number,
  target: oCNpc,
  attackerPlayer: Player,
  damage: number,
) => {
  if (enemyIndex == -1) {
    return
  }

  if (attackerPlayer) {
    attackerPlayer.SetAttrs({
      totalDamageDealt: attackerPlayer!.Attrs.totalDamageDealt + damage,
      waveDamageDealt: attackerPlayer!.Attrs.waveDamageDealt + damage,
    })
  }

  if (target.GetAttribute(NPC_ATR_HITPOINTS)! <= 0) {
    if (attackerPlayer) {
      attackerPlayer.SetAttrs({
        totalWaveEnemyKills: attackerPlayer.Attrs.totalWaveEnemyKills + 1,
        waveEnemyKills: attackerPlayer.Attrs.waveEnemyKills + 1,
      })
    }

    gameMode.state.addWaveEnemyToCorpseCollector(target)
    gameMode.state.removeCurrentWaveEnemy(enemyIndex)
    gameMode.state.addKilledEnemy()

    for (const p of gameMode.Players) {
      UpdateQuestOverlay(gameMode, p)
    }
  }
}
export const ManageEnemiesWithoutFocus = (state: GameState) => {
  if (!AreAllEnemiesFocusedOnAttack(state)) {
    for (const enemy of state.waveEnemiesWithoutAttackFocus) {
      MoveToGarond(state, enemy)
    }

    state.restartWaveEnemiesWithoutAttackFocus()
  }
}

export const SpawnWaveEnemy = (state: GameState) => {
  const spawn = GetRandomSpawnVector(ENEMY_SPAWN_MAP)

  InitializeWaveEnemy(state, state.currentWaveEnemyType, spawn!)
}

import { zVEC3 } from 'gothic-together'
import { oCMsgMovement } from 'gothic-together/union/classes/oCMsgMovement'
import { oCNpc } from 'gothic-together/union/classes/oCNpc'
import { TMovementSubType } from 'gothic-together/union/enums'
import { GameState } from 'src/store.js'
import { GameMode } from 'src/gamemode.js'
import { PREPARE_PHASE_TIME } from './difficulty.js'

export const GetRandomWaveEnemy = (waveEnemyArray: string[]) => {
  const randomIndex = Math.floor(Math.random() * waveEnemyArray.length)

  return waveEnemyArray[randomIndex]!
}

export const GetRandomSpawnVector = (enemySpawnMap: Map<string, zVEC3>) => {
  const keys = Array.from(enemySpawnMap.keys())
  const randomIndex = Math.floor(Math.random() * keys.length)
  const randomKey = keys[randomIndex]

  return enemySpawnMap.get(randomKey!)
}

export const MoveToNpc = (attackerNpc: oCNpc, targetNpc: oCNpc) => {
  const eventMan = attackerNpc!.eventManager()
  const walkModeMessage = oCMsgMovement.oCMsgMovement_OnInit6(TMovementSubType.EV_SETWALKMODE, 0)
  const gotoVobMessage = oCMsgMovement.oCMsgMovement_OnInit3(
    TMovementSubType.EV_GOTOVOB,
    targetNpc!,
  )

  eventMan!.OnMessage(walkModeMessage!, attackerNpc!)
  eventMan!.OnMessage(gotoVobMessage!, attackerNpc!)
}

export const AreAllEnemiesFocusedOnAttack = (state: GameState) => {
  return state.waveEnemiesWithoutAttackFocus.length == 0
}

export const CalculateDistanceBetweenNpc = (npc1: oCNpc, npc2: oCNpc) => {
  const vec1 = npc1.GetPositionWorld() as zVEC3
  const vec2 = npc2.GetPositionWorld() as zVEC3

  return CalculateDistance(vec1, vec2)
}

export const GetCurrentWaveEnemyIndex = (state: GameState, target: oCNpc) => {
  return state.currentWaveEnemies.findIndex((enemy) => enemy.Uuid == target.Uuid)
}

export const SelectNearbyPlayer = (gameMode: GameMode) => {
  const randomIndex = Math.floor(Math.random() * gameMode.state.possiblePlayersToAttack.length)

  return gameMode.state.possiblePlayersToAttack[randomIndex]
}

export const CalculateDistance = (vec1: zVEC3, vec2: zVEC3) =>
  Math.sqrt(
    Math.pow(vec2.X - vec1.X, 2) + Math.pow(vec2.Y - vec1.Y, 2) + Math.pow(vec2.Z - vec1.Z, 2),
  )

export const IsActivePhase = (state: GameState) => {
  return state.gamePhase == 'ACTIVE'
}

export const HasPreparePhaseEnded = (state: GameState) => {
  return (new Date().getTime() - state.lastWaveEndTime!.getTime()) / 1000 > PREPARE_PHASE_TIME
}

export const AreNpcsWithinDistance = (npc1: oCNpc, npc2: oCNpc, distance: number) => {
  return CalculateDistanceBetweenNpc(npc1!, npc2)! <= distance
}

export const AreWaveEnemiesDead = (state: GameState) => {
  return state.currentWaveEnemies.length == 0 && state.gamePhase == 'ACTIVE'
}


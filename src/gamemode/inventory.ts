import { Player } from 'gothic-together/player'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { AddItemToInventory, UseItemFromInventory } from 'src/inventory/index.js'
import { CreateItem } from 'src/inventory/item.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'

export const EquipStartItems = (player: Player) => {
  const mace = AddItemToInventory(player, CreateItem('ITMW_1H_BAU_MACE', 1))
  UseItemFromInventory(player, mace!.objectName)

  const bow = AddItemToInventory(player, CreateItem('ITRW_BOW_L_01', 1))
  UseItemFromInventory(player, bow!.objectName)

  AddItemToInventory(player, CreateItem('ITRW_ARROW', 5000))
  AddItemToInventory(player, CreateItem('ITRW_BOLT', 5000))

  const healthPotion = AddItemToInventory(player, CreateItem('ITPO_HEALTH_01', 3))
  const manaPotion = AddItemToInventory(player, CreateItem('ITPO_MANA_01', 3))
  const hotbarStartItems = [mace, bow, healthPotion, manaPotion, null, null, null, null, null]
  player.SetAttrs({ lastUsedWeapon: mace })

  UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
    hotbarSlots: hotbarStartItems,
  })

  player.SetAttrs({ hotbarSlots: hotbarStartItems })
}

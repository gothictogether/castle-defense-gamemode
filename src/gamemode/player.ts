import { Player } from 'gothic-together/player'
import { oCNpc } from 'gothic-together/union/classes/oCNpc'
import {
  NPC_ATR_HITPOINTS,
} from 'gothic-together/union/enums'
import { GameState } from 'src/store.js'
import { IsActivePhase } from './utils.js'
import { AreNpcsWithinDistance } from './utils.js'
import { NotifyAllPlayersAreDead } from './notifications.js'
import { UpdateQuestOverlay } from 'src/overlay/updater.js'
import { GameMode } from 'src/gamemode.js'
import { RestartGamemode } from 'src/utils/restart-gamemode.js'
import { UnequipWeapons } from 'src/inventory/index.js'
import { Client } from 'gothic-together'

export const ResetWaveDamageDealtAfterWave = (players: Player[]) => {
  for (const p of players) {
    p.SetAttrs({ waveDamageDealt: 0 })
    p.SetAttrs({ waveEnemyKills: 0 })
  }
}

export const UpdatePossiblePlayersToAttack = (gameMode: GameMode, npc: oCNpc, distance: number) => {
  for (const p of gameMode.Players) {
    const isNearby = AreNpcsWithinDistance(p.Npc, npc, distance)

    if (isNearby && !IsPlayerAlreadyPossibleToAttack(gameMode.state, p)) {
      gameMode.state.addPossiblePlayerToAttack(p.Npc)
    } else if (!isNearby && IsPlayerAlreadyPossibleToAttack(gameMode.state, p)) {
      gameMode.state.removePossiblePlayerToAttack(p.Npc)
    }
  }
}

export const CheckPlayersAliveStatus = (gameMode: GameMode) => {
  if (ArePlayersDead(gameMode.state, gameMode.Players)) {
    gameMode.state.setGamePhase('END')

    NotifyAllPlayersAreDead(gameMode.state, gameMode.Players)
    RestartGamemode(gameMode)
  }
}

export const ArePlayersDead = (state: GameState, players: Player[]) => {
  if (!IsActivePhase(state)) return

  for (const p of players) {
    const playerHp = p.Npc.GetAttribute(NPC_ATR_HITPOINTS)

    if (playerHp! > 0) {
      return false
    }
  }
  return true
}

export const GetRandomPossiblePlayerIndex = (state: GameState) => {
  return Math.floor(Math.random() * state.possiblePlayersToAttack.length)
}

export const HandlePlayerDeath = (
  gameMode: GameMode,
  attribute: number,
  currentValue: number,
  player: Player,
) => {
  if (attribute == NPC_ATR_HITPOINTS && currentValue == 0) {
    UnequipWeapons(player)
    UpdateQuestOverlay(gameMode, player)
  }
}

const IsPlayerAlreadyPossibleToAttack = (state: GameState, p: Player) => {
  return state.possiblePlayersToAttack.some((possibleNpc) => possibleNpc.Uuid === p.Npc.Uuid)
}

export const UpdatePlayerClientConfig = (player: Player) => {
  Client.ToggleClientConfig(player.Id, "default_bars", false)
  Client.ToggleClientConfig(player.Id, "default_inventory", false)
  Client.ToggleClientConfig(player.Id, "default_status_screen", false)
  Client.ToggleClientConfig(player.Id, "default_game_menu", false)
  Client.ToggleClientConfig(player.Id, "default_chat", true)
  Client.ToggleClientConfig(player.Id, "friendly_guild", true)
}
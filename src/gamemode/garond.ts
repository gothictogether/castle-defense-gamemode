import { Server, zVEC3 } from 'gothic-together'
import { oCNpc } from 'gothic-together/union/classes/oCNpc'
import { NPC_ATR_HITPOINTS, NPC_ATR_STRENGTH, NPC_HITCHANCE_1H } from 'gothic-together/union/enums'
import { GameMode } from 'src/gamemode.js'
import { UpdatePartyMemberOverlay, UpdateQuestOverlay } from 'src/overlay/updater.js'
import { RestartGamemode } from 'src/utils/restart-gamemode.js'

export const GAROND_SPAWN_VECTOR = new zVEC3([-2054.679, 177.803, -787.782])

export const SpawnGarond = (gamemode: GameMode) => {
  const routineFunc = Server.ParserGetIndex('ZS_STAND_WP') as number

  const garondNpc = Server.CreateNpc('Pal_250_Garond', GAROND_SPAWN_VECTOR) as oCNpc
  const garondWeapon = Server.PutInInventory("ITMW_1H_MISC_SWORD", garondNpc, 1)
  
  garondNpc.Equip(garondWeapon!)

  garondNpc.SetAttribute(NPC_ATR_STRENGTH, 30)
  garondNpc.SetHitChance(NPC_HITCHANCE_1H, 1)

  garondNpc.set_variousFlags(0)
  const state = garondNpc.state()
  gamemode.RoutineManager.RemoveRoutine(garondNpc)
  state?.InsertRoutine(8, 0, 23, 0, routineFunc, 'OC_EBR_GUARDPASSAGE_02', 0)
  state?.InsertRoutine(23, 0, 8, 0, routineFunc, 'OC_EBR_GUARDPASSAGE_02', 0)
  garondNpc.set_daily_routine(routineFunc)
  state?.InitRoutine()
  gamemode.update({ captainGarond: garondNpc })

  gamemode.RoutineManager.RestartRoutines()
}

export const HandleCaptainGarondState = (gamemode: GameMode, target: oCNpc) => {
  if (gamemode.state.captainGarond && target.Uuid == gamemode.state.captainGarond.Uuid) {
    UpdatePartyMemberOverlay(gamemode)
    if (gamemode.state.captainGarond.GetAttribute(NPC_ATR_HITPOINTS) == 0) {
      for (const p of gamemode.Players) {
        UpdateQuestOverlay(gamemode, p)
      }

      gamemode.state.setGamePhase('END')
      RestartGamemode(gamemode)
    }
  }
}

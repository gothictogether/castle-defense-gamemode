import { Player } from 'gothic-together/player'
import { Item } from 'react/src/utils/item-helpers.js'
import { OverlayState } from 'react/src/pages/Overlay/index.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'

export const RemoveItemFromHotbar = (player: Player, objectName: string) => {
  const hotbar: (Item | null)[] = player.Attrs.hotbarSlots

  const updatedHotbar = hotbar.map((i) => {
    return i?.objectName === objectName ? null : i
  })

  player.SetAttrs({ hotbarSlots: updatedHotbar })

  UpdateOverlayState<OverlayState>(player, 'OverlayComponent', {
    hotbarSlots: updatedHotbar,
  })
}

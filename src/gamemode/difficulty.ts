import { GameMode } from 'src/gamemode.js'
import { UpdateQuestOverlay } from 'src/overlay/updater.js'

export const BASE_GOLD_REWARD = 400
export const BASE_LP_REWARD = 20
export const BASE_ENEMY_SPAWN_LIMIT = 2
export const BASE_WAVE_ENEMY_COUNT = 3
export const NEXT_STAGE_WAVE_THRESHOLD = 6
export const PREPARE_PHASE_TIME = 60
export const ENEMY_SPAWN_MULTIPLIER_VALUE = 0.25
export const ENEMIES_PER_PLAYER_VALUE = 2

export const WAVE_ENEMY_STAGES: string[][] = [
  ['ywolf', 'ygiant_bug', 'YGobbo_Green'],
  ['scavenger', 'wolf', 'molerat', 'giant_bug', 'Bdt_1001_Bandit_L'],
  ['snapper', 'minecrawler', 'lurker', 'keiler'],
  ['Bloodhound', 'warg', 'minecrawlerwarrior', 'Waran'],
  ['OrcWarrior_Rest', 'OrcWarrior_Harad', 'razor'],
  ['Skeleton', 'Zombie04', 'OrcElite_Rest', 'Shadowbeast'],
  ['FireWaran', 'DragonSnapper', 'icewolf', 'Draconian', 'Skeleton_Lord'],
]

export const SetDifficulty = (gameMode: GameMode) => {
  gameMode.state.setEnemySpawnMultiplier(Math.sqrt(gameMode.Players.length * ENEMY_SPAWN_MULTIPLIER_VALUE))
  gameMode.state.setWaveEnemySpawnLimit(BASE_ENEMY_SPAWN_LIMIT + (gameMode.Players.length * ENEMIES_PER_PLAYER_VALUE))
  gameMode.state.setCurrentBaseWaveEnemyCount(BASE_WAVE_ENEMY_COUNT + gameMode.Players.length)

  if (gameMode.state.waveCounter == 0) {
    gameMode.state.setWaveEnemyCount(gameMode.state.currentBaseWaveEnemyCount)
    for (const p of gameMode.Players) {
      UpdateQuestOverlay(gameMode, p)
    }
  }
}
import { Player } from 'gothic-together/player'
import { GameState } from 'src/store.js'
import { Server, zVEC3 } from 'gothic-together'
import { WAVE_ENEMY_STAGES } from './difficulty.js'
import { NEXT_STAGE_WAVE_THRESHOLD } from './difficulty.js'
import {
  AreWaveEnemiesDead,
  HasPreparePhaseEnded,
  MoveToNpc,
} from './utils.js'
import { UpdateOverlayPreparePhase } from './updater.js'
import { ResetWaveDamageDealtAfterWave } from './player.js'
import { oCNpc } from 'gothic-together/union/classes/oCNpc'
import { DropRewardPotionFromSky, GrantWaveCompletionReward } from './reward.js'
import { ShowScoreboard } from 'src/utils/scoreboard.js'
import { GameMode } from 'src/gamemode.js'
import { UpdateQuestOverlay } from 'src/overlay/updater.js'
import { NotifyAllPlayersOnNextWave } from './notifications.js'
import { IsNextStageAvailable, SetNextStage } from './stage.js'
import { SetWaveEnemyType } from './enemy-handler.js'
import { PlayAudio } from 'src/utils/audio.js'

export const CheckWaveStatus = (gameMode: GameMode) => {
  if (AreWaveEnemiesDead(gameMode.state)) {
    gameMode.state.setGamePhase('PREPARE')

    gameMode.state.restartTimeLeftToEndPreparePhase()
    GrantWaveCompletionReward(gameMode.state, gameMode.Players)
    DropRewardPotionFromSky()
    ShowScoreboard(gameMode)
    InitializeNextWave(gameMode.state, gameMode.Players)
    SetWaveEnemyType(gameMode.state)

    ChangeEnemyTypeIndex(gameMode)
  }
}

export const InitializeNextWave = (state: GameState, players: Player[]) => {
  state.setLastWaveEndTime(new Date())
  state.nextWave()
  state.restartWaveEnemyIndex()
  state.restartCurrentWaveEnemiesKilled()
  state.restartPossiblePlayerToAttack()
  state.restartWaveEnemiesWithoutAttackFocus()
  state.resetVotesForPreparePhaseSkip()

  ResetWaveDamageDealtAfterWave(players)
}

export const HandlePreparePhase = (gameMode: GameMode) => {
  if (gameMode.state.gamePhase == 'PREPARE') {
    UpdateOverlayPreparePhase(gameMode)
    gameMode.state.decrementTimeLeftToEndPreparePhase()

    if (!HasPreparePhaseEnded(gameMode.state)) {
      return
    }

    if (IsNextStageAvailable(gameMode.state)) {
      SetNextStage(gameMode.state)
      SetWaveEnemyType(gameMode.state)
    }

    RemoveEnemyCorpsesFromWorld(gameMode.state)

    if (gameMode.state.waveCounter % NEXT_STAGE_WAVE_THRESHOLD != 0)
      gameMode.state.addWaveEnemyCount()

    gameMode.state.disposeWaveEnemyCorpses()

    for (const p of gameMode.Players) {
      UpdateQuestOverlay(gameMode, p)
    }
    SetWaveEnemyType(gameMode.state)
    NotifyAllPlayersOnNextWave(gameMode.state, gameMode.Players)
    PlayAudio(gameMode.Players, 'WaveStart.mp3')
    gameMode.state.setGamePhase('ACTIVE')
    gameMode.state.restartTimeLeftToEndPreparePhase()
  }
}

export const RemoveEnemyCorpsesFromWorld = (state: GameState) => {
  for (const corpse of state.waveEnemyCorpseCollector) {
    corpse.RemoveVobFromWorld()
  }
}

export const InitializeWaveEnemy = (state: GameState, waveEnemyName: string, spawnVector: zVEC3) => {
  const enemy = Server.CreateNpc(waveEnemyName, spawnVector)

  state.addWaveEnemyWithoutAttackFocus(enemy!)
  state.addCurrentWaveEnemy(enemy!)
}

export const MoveToGarond = (state: GameState, enemy: oCNpc) => {
  MoveToNpc(enemy, state.captainGarond!)
}

export const ChangeEnemyTypeIndex = (gameMode: GameMode) => {
  if (
    gameMode.state.currentWaveEnemyTypeIndex !=
    WAVE_ENEMY_STAGES[gameMode.state.waveEnemyStage]!.length - 1
  ) {
    gameMode.state.incrementCurrentWaveEnemyTypeIndex()
  } else {
    gameMode.state.resetCurrentWaveEnemyTypeIndex()
  }
}
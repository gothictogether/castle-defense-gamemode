import { Player } from 'gothic-together/player'
import {
  NPC_ATR_DEXTERITY,
  NPC_ATR_HITPOINTS,
  NPC_ATR_HITPOINTSMAX,
  NPC_ATR_MANA,
  NPC_ATR_MANAMAX,
  NPC_ATR_STRENGTH,
  oTEnumNpcTalent,
} from 'gothic-together/union/enums'

type PlayerAttributes = {
  hitPoints: number
  hitPointsMax: number
  mana: number
  manaMax: number
  strength: number
  dexterity: number
  oneHanded: number
  twoHanded: number
  bow: number
  cbow: number
  mageCircle: number
}

export const adminAttributes = {
  hitPoints: 5000,
  hitPointsMax: 5000,
  mana: 5000,
  manaMax: 5000,
  strength: 5000,
  dexterity: 5000,
  oneHanded: 100,
  twoHanded: 100,
  bow: 100,
  cbow: 100,
  mageCircle: 0
}

export const attributes = {
  hitPoints: 200,
  hitPointsMax: 200,
  mana: 200,
  manaMax: 200,
  strength: 10,
  dexterity: 10,
  oneHanded: 10,
  twoHanded: 10,
  bow: 10,
  cbow: 10,
  mageCircle: 0
}

export const InitializePlayerAttributes = (player: Player, attr: PlayerAttributes) => {
  player.Npc.SetAttribute(NPC_ATR_HITPOINTS, attr.hitPoints)
  player.Npc.SetAttribute(NPC_ATR_HITPOINTSMAX, attr.hitPointsMax)
  player.Npc.SetAttribute(NPC_ATR_MANA, attr.mana)
  player.Npc.SetAttribute(NPC_ATR_MANAMAX, attr.manaMax)
  player.Npc.SetAttribute(NPC_ATR_STRENGTH, attr.strength)
  player.Npc.SetAttribute(NPC_ATR_DEXTERITY, attr.dexterity)

  player.Npc.SetHitChance(oTEnumNpcTalent.NPC_TAL_1H, attr.oneHanded)
  player.Npc.SetHitChance(oTEnumNpcTalent.NPC_TAL_2H, attr.twoHanded)
  player.Npc.SetHitChance(oTEnumNpcTalent.NPC_TAL_BOW, attr.bow)
  player.Npc.SetHitChance(oTEnumNpcTalent.NPC_TAL_CROSSBOW, attr.cbow)
  player.Npc.SetTalentSkill(oTEnumNpcTalent.NPC_TAL_MAGE, attr.mageCircle)
  player.Npc.SetTalentValue(oTEnumNpcTalent.NPC_TAL_MAGE, attr.mageCircle)

  player.Npc.SetTalentSkill(oTEnumNpcTalent.NPC_TAL_1H, 0)
  player.Npc.SetTalentValue(oTEnumNpcTalent.NPC_TAL_1H, 0)

  player.Npc.SetTalentSkill(oTEnumNpcTalent.NPC_TAL_2H, 0)
  player.Npc.SetTalentValue(oTEnumNpcTalent.NPC_TAL_2H, 0)

  player.Npc.SetTalentSkill(oTEnumNpcTalent.NPC_TAL_BOW, 0)
  player.Npc.SetTalentValue(oTEnumNpcTalent.NPC_TAL_BOW, 0)

  player.Npc.SetTalentSkill(oTEnumNpcTalent.NPC_TAL_CROSSBOW, 0)
  player.Npc.SetTalentValue(oTEnumNpcTalent.NPC_TAL_CROSSBOW, 0)
  
  const model = player.Npc.GetModel()
  model?.ApplyModelProtoOverlay(`humans`)
}

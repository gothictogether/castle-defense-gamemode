import { Player } from 'gothic-together/player'
import { AddItemToInventory } from 'src/inventory/index.js'
import { UpdateOverlayState } from 'src/utils/update-overlay-state.js'
import { HeroPageState } from 'react/src/pages/Hero/index.js'
import { GameState } from 'src/store.js'
import { BASE_GOLD_REWARD, BASE_LP_REWARD } from './difficulty.js'
import { CreateItem } from 'src/inventory/item.js'
import { Server, zVEC3 } from 'gothic-together'

export const GrantWaveCompletionReward = (gameState: GameState, players: Player[]) => {
  for (const player of players) {
    const goldReward = CalculateReward(BASE_GOLD_REWARD, gameState, player)
    const learningPointsReward = CalculateReward(BASE_LP_REWARD, gameState, player)

    player.SetAttrs({
      learningPoints: player.Attrs.learningPoints + learningPointsReward,
    })

    AddItemToInventory(player, CreateItem('ITMI_GOLD', goldReward))

    UpdateOverlayState<HeroPageState>(player, 'HeroComponent', {
      lp: player.Attrs.learningPoints,
    })
  }
}

export const GrantBaseRewardOnJoin = (player: Player, gameState: GameState) => {
  const goldAmount = BASE_GOLD_REWARD * gameState.waveCounter
  
  player.SetAttrs({ learningPoints: BASE_LP_REWARD * gameState.waveCounter })
  
  AddItemToInventory(player, CreateItem('ITMI_GOLD', goldAmount))

  UpdateOverlayState<HeroPageState>(player, 'HeroComponent', {
    lp: player.Attrs.learningPoints,
  })
}

const CalculateReward = (baseValue: number, gameState: GameState, player: Player): number => {
  const totalEnemies = gameState.waveEnemyCount
  return Math.floor(baseValue + (baseValue * player.Attrs.waveEnemyKills) / totalEnemies!)
}

export const DropRewardPotionFromSky = () => {
  Server.CreateItem("ITPO_PERM_HEALTH", new zVEC3([-1636.045, 1400.162, -309.314]))
  Server.CreateItem("ITPO_PERM_HEALTH", new zVEC3([-1927.969, 1210.162, 374.046]))
}
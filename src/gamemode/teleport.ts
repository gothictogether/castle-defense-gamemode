import { GameMode } from 'src/gamemode.js'
import { CalculateDistance } from './utils.js'
import { LOST_NPCS_RESPAWN_VECTOR, ENEMY_TELEPORT_DISTANCE_THRESHOLD } from './wave-constants.js'

export const TeleportEnemyFarAwayToHome = (gameMode: GameMode) => {
  for (const e of gameMode.state.currentWaveEnemies) {
    if (
      CalculateDistance(e.GetPositionWorld()!, LOST_NPCS_RESPAWN_VECTOR) >
      ENEMY_TELEPORT_DISTANCE_THRESHOLD
    ) {
      e.SetCollDet(0)
      e.SetPositionWorld(LOST_NPCS_RESPAWN_VECTOR)
      e.SetCollDet(1)
    }
  }
}

export const TeleportGarondFarAwayToHome = (gameMode: GameMode) => {
  const captainGarond = gameMode.state.captainGarond

  if (
    CalculateDistance(gameMode.state.captainGarond!.GetPositionWorld()!, LOST_NPCS_RESPAWN_VECTOR) >
    ENEMY_TELEPORT_DISTANCE_THRESHOLD
  ) {
    captainGarond!.SetCollDet(0)
    captainGarond!.SetPositionWorld(LOST_NPCS_RESPAWN_VECTOR)
    captainGarond!.SetCollDet(1)
  }
}
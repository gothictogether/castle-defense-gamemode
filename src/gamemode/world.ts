import { Client, zVEC3 } from "gothic-together"
import { Player } from "gothic-together/player"

export const RemoveDefaultVobs = (player: Player) => {
    Client.RemovePreexistingObject(player.Id, 'OC_BARRELS_REIHE.3DS', new zVEC3([-945.31, 101.035, -814.381]))
    Client.RemovePreexistingObject(player.Id, 'AW_CRATEPILE_01.3DS', new zVEC3([-967.761, 251.745, 18.3329]))
}
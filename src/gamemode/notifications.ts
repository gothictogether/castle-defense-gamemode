import { Player } from 'gothic-together/player'
import { GameState } from 'src/store.js'
import { ShowNotification } from 'src/utils/notifications.js'

export const NotifyAllPlayersOnNextWave = (state: GameState, players: Player[]) => {
  ShowNotification(
    players,
    `Nadciąga ${state.waveCounter} Fala`,
    'Nadchodzi kolejne natarcie przeciwnika!',
    3,
  )
}

export const NotifyAllPlayersAreDead = (state: GameState, players: Player[]) => {
  ShowNotification(
    players,
    'Wszyscy gracze nie żyją!',
    `Liczba przetrwanych fal: ${state.waveCounter}`,
    5,
  )
}

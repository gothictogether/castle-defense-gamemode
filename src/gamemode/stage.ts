import { GameState } from 'src/store.js'
import { NEXT_STAGE_WAVE_THRESHOLD, WAVE_ENEMY_STAGES } from './difficulty.js'

export const IsNextStageAvailable = (state: GameState) => {
  return (
    state.waveEnemyStage < WAVE_ENEMY_STAGES.length - 1 &&
    state.waveCounter % NEXT_STAGE_WAVE_THRESHOLD == 0
  )
}

export const SetNextStage = (state: GameState) => {
  if (state.waveEnemyStage < WAVE_ENEMY_STAGES.length) {
    state.setWaveEnemyCount(state.currentBaseWaveEnemyCount)
    state.nextWaveEnemyStage()
    state.resetCurrentWaveEnemyTypeIndex()
  }
}
import { Player as CorePlayer } from 'gothic-together/player'
import { store } from './store.js'
import { Item } from './inventory/item.js'

export type PlayerAttributes = {
  readonly learningPoints: number
  readonly healTargetUuid: string
  readonly totalDamageDealt: number
  readonly waveDamageDealt: number
  readonly totalWaveEnemyKills: number
  readonly avatarUrl: string
  readonly totalWavesSurvived: number
  readonly reviveTimeStart: number
  readonly role: 'player' | 'admin'
  readonly waveEnemyKills: number
  readonly hotbarSlots: (Item | null)[]
  readonly lastUsedWeapon: Item | null
}

export class MyPlayer extends CorePlayer {
  override get Attrs() {
    return store.getState().players[this.Id] as PlayerAttributes
  }

  override SetAttrs(attr: Partial<PlayerAttributes>) {
    return store.getState().updatePlayerAttributes(this.Id, attr)
  }
}

import './env.js'
import 'source-map-support/register'

import { SetGameMode, StartGameMode, SetPlayerClass, SetMiddlewares } from 'gothic-together'
import { GameMode } from './src/gamemode.js'
import { MyPlayer } from 'src/player.js'
import { RevivePlayerMiddleware } from 'src/revive/middleware.js'
import { InventoryMiddleware } from 'src/inventory/middleware.js'

import { AddCommandHandlers } from 'src/utils/commands-handler.js'
import AdminCommands from 'src/admin/commands.js'
import ReviveCommands from 'src/revive/commands.js'
import HeroCommands from 'src/hero/commands.js'
import InventoryCommands from 'src/inventory/commands.js'
import CreatorCommands from 'src/creator/commands.js'
import OverlayCommands from 'src/overlay/commands.js'
import ShopCommands from 'src/shop/commands.js'
import HotBarCommands from 'src/hotbar/commands.js'

console.log(`Node started in ${process.env['APP_ENV']} mode.`)

const gameMode = new GameMode()
const revivePlayerMiddleware = new RevivePlayerMiddleware(gameMode)
const inventoryMiddleware = new InventoryMiddleware(gameMode)

SetGameMode(gameMode)
SetPlayerClass(MyPlayer)
SetMiddlewares([revivePlayerMiddleware, inventoryMiddleware])

AddCommandHandlers({
  ...AdminCommands,
  ...ReviveCommands,
  ...HeroCommands,
  ...InventoryCommands,
  ...CreatorCommands,
  ...OverlayCommands,
  ...ShopCommands,
  ...HotBarCommands
})

StartGameMode()
